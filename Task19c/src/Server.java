import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 05.11.15.
 */
public class Server {
    public static void main(String[] args) throws IOException {
        final int PORT = 8080;
        ServerSocket s = new ServerSocket(PORT);
        System.out.print("Waiting for connection.....");
        Socket client = s.accept();
        System.out.println("Connected.");
        PrintWriter os = new PrintWriter(client.getOutputStream(), true);
        BufferedReader is =new BufferedReader( new InputStreamReader(client.getInputStream()));
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your name:");
        String sname = scanner.nextLine();
        os.println(sname);
        String cname = is.readLine();
        while (true) {
            System.out.print(sname+":");
            String b = scanner.nextLine();
            os.println(b);
            String x = is.readLine();
            System.out.println(cname+": " + x);
        }
    }
}
