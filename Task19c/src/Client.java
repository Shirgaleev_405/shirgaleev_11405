import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 05.11.15.
 */
public class Client {
    public static void main(String[] args) throws IOException {
        int port = 8080;
        String host = "localhost";
        Socket s = new Socket(host, port);
        PrintWriter os = new PrintWriter(s.getOutputStream(), true);
        BufferedReader is = new BufferedReader(new InputStreamReader(s.getInputStream()));
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name:");
        String sname = is.readLine();
        String cname = scanner.nextLine();
        os.println(cname);
        System.out.println("Message waiting....");

        while (true) {
            String x = is.readLine();
            System.out.println(sname+":" + x);
            System.out.print(cname+":");
            String b = scanner.nextLine();
            os.println(b);
        }
    }
}
