import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by salavatshirgaleev on 29.03.15.
 */
public class BucketSort {
    private static int iteration;

    static int[] sort(int[] sequence, int maxValue) {

        int[] Bucket = new int[maxValue + 1];
        int[] sorted_sequence = new int[sequence.length];

        for (int i = 0; i < sequence.length; i++) {
            Bucket[sequence[i]]++;
        }
        int s = 0;
        int outPos = 0;
        for (int i = 0; i < Bucket.length; i++) {
            for (int j = 0; j < Bucket[i]; j++) {
                sorted_sequence[outPos++] = i;
                s++;
                iteration++;
            }
        }
        System.out.println(s);
        return sorted_sequence;

    }

    static void printSequence(int[] sorted_sequence) {
        for (int i = 0; i < sorted_sequence.length; i++) {
            System.out.print(sorted_sequence[i] + " ");
            iteration++;
        }

    }

    static int maxValue(int[] sequence) {
        int maxValue = 0;
        for (int i = 0; i < sequence.length; i++) {
            if (sequence[i] > maxValue) {
                maxValue = sequence[i];
                iteration++;
            }
        }
        return maxValue;
    }
    public static void createFilesAndSort() throws IOException {
        File logs = new File("LogsOfSem_Work_2.txt");
        logs.createNewFile();
        PrintWriter pwForLogs = new PrintWriter(logs.getAbsoluteFile());
        Random ran = new Random();
        for (int k = 10; k < 80; k = k + 80) { // Шаг = 80.
            String name = Integer.toString(k);
            File fileForWork = new File("File" + name + ".txt");
            fileForWork.createNewFile();
            PrintWriter pw = new PrintWriter(fileForWork.getAbsoluteFile());

            int[] sequence = new int[k];
            for (int i = 0; i < k; i++) {
                sequence[i] = ran.nextInt((k));
                pw.print(sequence[i] + "\n");
            }
            pw.close();

            long worktime = System.nanoTime();
            int maxValue = maxValue(sequence);
            printSequence(sort(sequence, maxValue));
            worktime = System.nanoTime() - worktime;

            pwForLogs.println("For " + k + " elements, Time = " + worktime + " nanoseconds;");
            pwForLogs.println("Amount of Iters = " + iteration + ".");
            pwForLogs.println();

            System.out.println("For " + k + " elements, Time = " + worktime + " nanoseconds;");
            System.out.println("Amount of Iters = " + iteration + ".");
            System.out.println();
            iteration = 0;
        }
        pwForLogs.close();
    }

    public static void main(String[] args) throws IOException {
        createFilesAndSort();
    }

}