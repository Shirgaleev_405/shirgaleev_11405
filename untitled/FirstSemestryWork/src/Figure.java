import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 25.02.15.
 */
public class Figure {

    private int f, x1, y1, x2, y2, colour;
    public Figure next;


    public int getX1() {
        return x1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public int getY1() {
        return y1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public int getColour() {
        return colour;
    }

    public void setColour(int colour) {
        this.colour = colour;
    }

    public void set(Figure next) {
    }

    public Figure getNext() {
        return next;

    }


    public void setNext(Figure next) {
        this.next = next;

    }

    public Figure(int f, int x1, int y1, int x2, int y2, int colour) {
        this.f = f;
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.colour = colour;
        this.next = next;
    }


    public int getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }

    public double square() {
        if (this.f == 1) {
            return Math.abs((this.x2 - this.x1) * (this.y2 - this.y1));
        }
        if (this.f == 3) {
            return Math.PI * this.x2 * this.x2;
        } else {
            return 0;
        }

    }

}