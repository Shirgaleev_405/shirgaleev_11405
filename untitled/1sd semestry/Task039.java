import java.util.Random;
import java.util.Scanner;

/**
 * @author ShirgaleevSalavat
 *
 */
public class Task039 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Razmer massiva");
        int p = scanner.nextInt();
        Random ran = new Random();
        System.out.println("Razmer matrici");
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int[][] a = new int[m][n];
        int[] b = new int[p];

        System.out.println("Elementi massiva");
        for (int i = 0; i < p; i++) {
            b[i] = scanner.nextInt();
        }
        System.out.println("Massiv:");
        for (int i = 0; i < p; i++) {
            System.out.print(b[i] + " ");
        }
        System.out.println();
        System.out.println();
        System.out.println("Matrica:");
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < n; i++) {
                a[j][i] = ran.nextInt(10);
                System.out.print(a[j][i] + " ");
            }
            System.out.println();
        }
        System.out.println();
        System.out.println("Vivod:");
        for (int i = 0; i < p; i++) {
            System.out.print(b[i] + ":  HET");
            System.out.println();
        }
        System.out.println();
    }
}