import java.util.Scanner;

/**
 * Created by Salavat on 21.10.2014.
 */
public class Task033 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        double sum = 0;
        int k = 0;
        int q = 0;
        double cos = 0;
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int[] b = new int[n];
        for (int i = 0; i < n; i++) {
            b[i] = scanner.nextInt();
        }
        for (int i = 0; i < n; i++) {
            sum = sum + a[i] * b[i];
            k = k + a[i] * a[i];
            q = q + b[i] * b[i];
        }
        cos = sum / Math.sqrt(k) / Math.sqrt(q);
        System.out.println(sum + "cos=" +cos );

    }
}
