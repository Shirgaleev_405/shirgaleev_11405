/**
 * Created by Salavat on 05.12.2014.
 */
public class Round implements Perimetr, Measurable {
    private int r;

    public Round(int r) throws GeometricException {
        if (r < 0)  {
            throw new GeometricException("Недопустимое значение");
        }
        this.r = r;
    }


    @Override
    public double measurable() {
        return Math.PI * r * r;
    }

    @Override
    public double perimetr() {
        return 2 * Math.PI * r;
    }

}
