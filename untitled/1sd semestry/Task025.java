/**
 * @author Salavat Shirgaleev
 *         405
 *         025
 */
public class Task025 {
    public static void main(String[] args) {
        final double EPS = 1e-9;
        double sum = 0;
        double num = 1;
        double den = 1;
        double dev = 1;
        double n1 = 1;
        for (double n = 1; Math.abs(dev) > EPS; n++) {
            den = n * n + 3 * n;
            dev = n1 / den;
            sum = sum + dev;
            n1 = n1 * (-1);
        }
        System.out.println(sum);
    }
}
