import java.util.Scanner;

/**
 * @author Salavat Shirgaleev
 *         405
 *         023
 */
public class Task026 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        final double EPS = 1e-9;
        double num = 1;
        double den = 1;
        double sum = 0;
        double dev = 1;
        double i = 1;
        double t = 1;
        double f = 1;
        for (double n = 1; dev >= EPS; n++) {
            i = (x - 1) * i;
            t = 3 * t;
            f = f * n;
            den = t * (n * n + 3) * f;
            dev = i / den;
            sum = sum + dev;
        }
        System.out.println(sum);
    }
}
