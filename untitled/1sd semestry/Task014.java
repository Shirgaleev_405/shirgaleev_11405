import java.util.Scanner;

/**
 * Created by Салават on 21.09.2014.
 */
public class Task014 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double x = scanner.nextInt();
        double p = 0;
        p = Math.cos(x);
        for(int i=1;i <= n;i++) {
            p = Math.cos(x + p);
        }
        System.out.println(p);
    }
}
