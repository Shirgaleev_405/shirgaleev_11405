/**
 * Created by Salavat on 05.12.2014.
 */
public class Rectangle implements Perimetr, Measurable {
    private int a, b;

    public Rectangle(int a, int b) throws GeometricException {
        if ((a < 0) || (b < 0)) {
            throw new GeometricException("Неверные значения");
        }
        this.a = a;
        this.b = b;
    }

    @Override
    public double measurable() {
        return a * b;
    }

    @Override
    public double perimetr() {
        return 2 * (a + b);
    }

    public static void main(String[] args) throws GeometricException {
        Rectangle r = new Rectangle(5, 5);
        System.out.println(r.perimetr());
    }
}


