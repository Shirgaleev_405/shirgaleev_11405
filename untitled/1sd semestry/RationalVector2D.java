package Task053;

import Task049.Vector2D;
import Task050.RationalFraction;

/**
 * Created by Salavat on 21.11.2014.
 */
public class RationalVector2D {
    private RationalFraction x;
    private RationalFraction y;

    public RationalVector2D(RationalFraction x, RationalFraction y) {
        this.x = x;
        this.y = y;
    }

    public RationalVector2D() {
        this(new RationalFraction(), new RationalFraction());
    }

    public RationalVector2D add(RationalVector2D n) {
        RationalVector2D n1 = new RationalVector2D();
        n1.x = this.x.add(n.x);
        n1.y = this.y.add(n.y);
        return n1;
    }

    public String toString() {
        return "{" + x + "," + y + "}";
    }

    public double Length() {
        RationalVector2D k = new RationalVector2D();
        k.x = this.x.mult(this.x);
        k.y = (this.y.mult(this.y));
        return Math.sqrt(k.x.value() + k.y.value());
    }


    public RationalFraction scalarProduct(RationalVector2D n) {
        return this.x.mult(n.x).add(this.y.mult(n.y));
    }

    public boolean equals(RationalVector2D n) {
        return (this.x.equals(n.x)) && (this.y.equals(n.y));
    }


}
