/**
 * Created by Salavat on 19.11.2014.
 */
public class RationalFraction {
    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    private int n;

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    private int d;

    public RationalFraction() {
        this(0, 1);
    }

    public RationalFraction(int n, int d) {
        this.n = n;
        this.d = d;
    }

    public void reduce() {
        int a = Math.abs(this.n);
        int b = Math.abs(this.d);
        int k = 1;

        while (a != 0 && b != 0) {
            if (a > b) {
                a %= b;
            } else {
                b %= a;
            }
            k = a + b;
        }
        this.n = this.n / k;
        this.d = this.d / k;

        if (d < 0) {
            this.d = -this.d;
            this.n = -this.n;
        }

    }

    public RationalFraction add(RationalFraction k) {
        RationalFraction t = new RationalFraction();
        t.n = this.n * k.d + k.n * this.d;
        t.d = this.d * k.d;
        t.reduce();
        return t;
    }

    public String toString() {
        return n + "/" + d;
    }

    public void add2(RationalFraction k) {
        this.n = this.n * k.d + k.n * this.d;
        this.d = this.d * k.d;
        this.reduce();
    }

    public RationalFraction sub(RationalFraction k) {
        RationalFraction t = new RationalFraction();
        t.n = this.n * k.d - k.n * this.d;
        t.d = this.d * k.d;
        t.reduce();
        return t;
    }

    public void sub2(RationalFraction k) {
        this.n = this.n * k.d - k.n * this.d;
        this.d = this.d * k.d;
        this.reduce();
    }

    public RationalFraction mult(RationalFraction k) {
        RationalFraction t = new RationalFraction();
        t.n = this.n * k.n;
        t.d = this.d * k.d;
        t.reduce();
        return t;
    }

    public void moult2(RationalFraction k) {
        this.n = this.n * k.n;
        this.d = this.d * k.d;
        this.reduce();
    }

    public RationalFraction div(RationalFraction k) {
        RationalFraction t = new RationalFraction();
        t.n = this.n * k.d;
        t.d = this.d * k.n;
        t.reduce();
        return t;
    }

    public void div2(RationalFraction k) {
        this.n = this.n * k.d;
        this.d = this.d * k.n;
        this.reduce();
    }

    public boolean equals(RationalFraction k) {
        this.reduce();
        k.reduce();
        return ((this.n == k.n) && (this.d == k.d));
    }

    public int numberPart() {
        return (this.n / this.n);
    }

    public double value() {
        return (this.n / this.d);
    }
}
