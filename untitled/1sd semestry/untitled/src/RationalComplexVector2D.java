/**
 * Created by Salavat on 07.12.2014.
 */
public class RationalComplexVector2D {
    RationalComplexNumber s1;
    RationalComplexNumber s2;
    public RationalComplexNumber getS1(){
        return this.s1;
    }
    public RationalComplexNumber getS2(){
        return this.s2;
    }
    public void setS1(RationalComplexNumber s1){
        this.s1 = s1;
    }
    public void setS2(RationalComplexNumber s2){
        this.s2 = s2;
    }
    public RationalComplexVector2D(){
        this(new RationalComplexNumber(), new RationalComplexNumber());
    }
    public RationalComplexVector2D(RationalComplexNumber s1,RationalComplexNumber s2){
        this.s1 = s1;
        this.s2 = s2;
    }
    public RationalComplexVector2D add(RationalComplexVector2D vector){
        RationalComplexVector2D c = new RationalComplexVector2D();
        c.s1 = this.s1.add(vector.s1);
        c.s2 = this.s2.add(vector.s2);
        return c;
    }
    public String toString(){
        return "{ " + this.s1 + " , " + this.s2 + " }";
    }
    public RationalComplexNumber scalarProduct(RationalComplexVector2D vector){
        return this.s1.mult(vector.s1).add(this.s2.mult(vector.s2));
    }
}
