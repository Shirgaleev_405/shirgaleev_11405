import java.util.Scanner;

/**
 * @author Salavat Shirgaleev
 *         405
 *         024
 */
public class Task024 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        final double EPS = 1e-9;
        double sum = 0;
        double dev = 1;
        int i = 1;
        double f = x - 1;
        for (int n = 1; dev >= EPS; n++, i++) {
            i = 9 * i;
            f = f * f * n;
            dev = n * i * f;
            sum = sum + (1 / dev);
        }
        System.out.println(sum);
    }
}
