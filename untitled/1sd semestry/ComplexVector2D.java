package Task054;


import Task051.ComplexNumber;

/**
 * Created by Salavat on 21.11.2014.
 */
public class ComplexVector2D {

    private ComplexNumber x;
    private ComplexNumber y;

    public ComplexVector2D(ComplexNumber x, ComplexNumber y) {
        this.x = x;
        this.y = y;
    }

    public ComplexVector2D() {
        this(new ComplexNumber(), new ComplexNumber());
    }

    public ComplexVector2D add(ComplexVector2D n) {
        ComplexVector2D n1 = new ComplexVector2D();
        n1.x = this.x.add(n.x);
        n1.y = this.y.add(n.y);
        return n1;
    }

    public String toString() {
        return "{" + x + "," + y + "}";
    }


    public ComplexNumber scalarProduct(ComplexVector2D n) {
        return this.x.multi(n.x).add(this.y.multi(n.y));
    }

    public boolean equals(ComplexVector2D n) {
        return (this.x.equals(n.x)) && (this.y.equals(n.y));
    }


}
