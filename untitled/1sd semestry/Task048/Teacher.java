/**
 * Created by Salavat on 13.11.2014.
 */
public class Teacher {
    private String fio;
    private String subject;


    public Teacher(String fio, String subject) {
        this.fio = fio;
        this.subject = subject;

    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFio() {

        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }


    public void evaluateMark(Student h) {
        int m = (int) (2 + Math.random() * 4);
        String s;
        if (m == 2) {
            s = "неудовлетворительно";
        } else if (m == 3) {
             s = "удовлетворительно";
        } else if (m == 4) {
            s = "хорошо";
        } else {
            s = "отлично";
        }
        System.out.println("Преподоватьель "
                + fio + " оценил студента с именем " + h.getFio() +
                " по предмету " + subject + " на оценку " + s);

    }

}
