import java.util.Scanner;

/**
 * Created by Salavat on 13.11.2014.
 */
public class Task048 {
    public static void main(String[] args) {
        Student s = new Student("Ширгалеев С.В.", 1996);
        Teacher v = new Teacher("Абрамский М.М.", "Программирование");
        System.out.println(v.getFio());
        v.setFio("");
        System.out.println(v.getSubject());
        v.setSubject("");
        v.evaluateMark(s);
    }
}
