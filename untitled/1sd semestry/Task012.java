import java.util.Scanner;

/**
 * Created by Салават on 21.09.2014.
 */
public class Task012 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double p = 0.0;
        for (int i = 1; i <= n; i += 2) {
            p = p + 1 / i / i - 1 / (i + 2) / (i + 2);
        }

        System.out.println(p);

    }
}
