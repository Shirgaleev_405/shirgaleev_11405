import java.util.Random;
import java.util.Scanner;

/**
 * @author ShirgaleevSalavat
 *
 */
public class Task038 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random ran = new Random();
        int n = scanner.nextInt();
        int[][] a = new int[n][n];
        int k = 1;
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++) {
                a[j][i] = ran.nextInt(10);
                System.out.print(a[j][i] + " ");
            }
            System.out.println();
        }
        System.out.println();
        for (int j = 1; j < n; j++) {
            for (int i = 0; i < k; i++) {
                a[j][i] = 0;
            }
            k++;
        }
        for (int j = 0; j < n; j++) {
            for (int i = 0; i < n; i++) {
                System.out.print(a[j][i] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }
}

