package Task050;

/**
 * Created by Salavat on 19.11.2014.
 */
public class TestRationalFraction {
    public static void main(String[] args) {
        RationalFraction r = new RationalFraction(2, 2);
        RationalFraction d = new RationalFraction(0, 2);
        r.reduce();
        System.out.println(r);
        System.out.println(r.add(d));
        System.out.println(r.toString());
        r.add2(d);
        r.reduce();
        System.out.println(r);
        System.out.println(r.sub(d));
        r.sub2(d);
        System.out.println(r);
        System.out.println(r.mult(d));
        r.mult2(d);
        System.out.println(r);
        System.out.println(r.div(d));
        r.div2(d);
        System.out.println(r);
        System.out.println(r.equals(d));
        System.out.println(r.numberPart());
        System.out.println(r.value());


    }
}
