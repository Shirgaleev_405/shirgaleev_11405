import java.util.Scanner;

/**
 * Created by Salavat on 21.10.2014.
 */
public class Task034 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] a = new int[n];
        int max = 0;
        int sum = 0;
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        for (int i = 1; i < n-1; i++) {
            sum = a[i - 1] + a[i] + a[i + 1];
            if (sum > max) {
                max = sum;
            }

        }

        System.out.println(max);
    }
}
