/**
 * Created by Salavat on 04.12.2014.
 */
public class Animal {
    public static void main(String[] args) {
        final int n = 10;
        Voiceable[] animal = new Voiceable[n];
        for (int i = 0; i < n; i += 2) {
            animal[i + 1] = new Dog();
            animal[i] = new Cat();
        }
        for (int i = 0; i < n; i += 2) {
            animal[i].voice();
        }
    }
}
