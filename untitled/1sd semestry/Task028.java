import java.util.Scanner;

/**
 * Created by Салават on 11.10.2014.
 */
public class Task028 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int m = scanner.nextInt();
        int min = Math.min(k, m);
        while (min % 3 != 0){
            min++;
        }
        while (min <= Math.max(k, m) ){
            System.out.println(min);
            min += 3;
        }
    }
}
