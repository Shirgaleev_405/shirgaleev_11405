import java.util.Scanner;

/**
 * Created by Салават on 11.10.2014.
 */

public class Task030 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int b = 0;
        boolean p = false;
        while (n > 0 && !p) {
            int x = scanner.nextInt();
            int a = 0;
            int c = x % 10;
            if (c % 2 == 0) {
                while (x > 0 && c % 2 == 0) {
                    a = a + 1;
                    x = x / 10;
                    c = x % 10;

                }
            } else {
                while (x > 0 && c % 2 != 0) {
                    a = a + 1;
                    x = x / 10;
                    c = x % 10;
                }
            }
            if (a == 3 | a == 5) {
                b = b + 1;
            }
            n--;
        }
        if (b == 2) {
            p = true;
        }
        System.out.println(p);
    }
}
