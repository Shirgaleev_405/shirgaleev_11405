import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by salavatshirgaleev on 17/04/15.
 */
public class File {
    public static void main(String[] args) throws IOException {
        int n;
        FileInputStream file = new FileInputStream("numberO.txt");
        FileOutputStream file1 = new FileOutputStream("numberC.txt");
        long a = System.nanoTime();
        while ((n = file.read()) != -1) {
            file1.write(n);
        }
        file.close();
        file1.close();
        long b = System.nanoTime();
        System.out.println(b - a);

        FileInputStream fil = new FileInputStream("numberO.txt");
        FileOutputStream fil1 = new FileOutputStream("numbert.txt");
        byte[] buf = new byte[10];
        int t = fil.read(buf);
        while (buf.length == t) {
            fil1.write(buf);
        }
        for (int i = 0; i < t; i++) {
            fil1.write(buf[i]);
        }
        fil.close();
        fil1.close();
    }
}


