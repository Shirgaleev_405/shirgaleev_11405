/**
 * Created by salavatshirgaleev on 09.02.15.
 */
public class ElemP<T> {

    ElemP<T> next;
    T value;

    public ElemP(T value, ElemP next) {
        this.value = value;
        this.next = next;
    }

    public ElemP() {
    }

    public ElemP<T> getNext() {
        return next;
    }

    public void setNext(ElemP next) {
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }


    public void set(int i) {
    }
}