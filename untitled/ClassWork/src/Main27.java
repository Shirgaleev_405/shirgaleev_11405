import java.io.*;

/**
 * Created by salavatshirgaleev on 27/04/15.
 */
public class Main27 {
    public static void main(String[] args) throws IOException {
        FileReader r=new FileReader("Original.txt");
        FileWriter w=new FileWriter("Copy.txt");
        int n =r.read();
        long a = System.nanoTime();
        while (n!=-1){
            w.write(n);
            n=r.read();
        }
        long b = System.nanoTime();
        System.out.println(b-a);
        r.close();
        w.close();


        BufferedReader br = new BufferedReader(r);


    }
}
