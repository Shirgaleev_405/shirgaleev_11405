/**
 * Created by salavatshirgaleev on 23.03.15.
 */
public class Node {
    public void setValue(int value) {
        this.value = value;
    }

    private int value;

    public void setLeft(Node left) {
        this.left = left;
    }

    private Node left;

    public void setRight(Node right) {
        this.right = right;
    }

    private Node right;

    public void setColor(int color) {
        this.color = color;
    }

    private int color;

    public Node(int i) {
        value = i;
    }

    public Node(int i, Node left, Node right,int colour) {
        this.value = i;
        this.left = left;
        this.right = right;
        this.color=colour;
    }

    public int getValue() {
        return value;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }

    public int getColor() {
        return color;
    }
}
