/**
 * Created by salavatshirgaleev on 05.03.15.
 */
public class MyLinkedQueue<T> implements MyQueue<ElemP> {
    private ElemP head;
    private ElemP tail;

    @Override
    public void offer(ElemP a) {
        ElemP p = new ElemP(a, null);
        if (head == null) {
            head = p;
            tail = p;
        } else {
            tail.setNext(p);
            tail = p;
        }
    }

    @Override
    public ElemP peek() {
        if(head!=null){
            return head;
        }else{
            return null;
        }

    }

    @Override
    public ElemP poll() {
        if(head!=null){
            return head;
        }else{
            return null;
        }
    }

    @Override
    public boolean isEmpty() {
        if (head!=null){
            return false;
        } else {
            return true;
        }
    }


}
