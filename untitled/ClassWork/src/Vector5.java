/**
 * Created by salavatshirgaleev on 13.04.15.
 */
public class Vector5 {


    public static void baskTrackingVector(String s, int n) {

        if (s.length() == n) {
            System.out.println("УРА!"+s);
        } else {
            for (int x=0;x<3;x++) {
                s=s+x;
                if (!s.contains("11")) {
                    baskTrackingVector(s, n);
                }
            }
        }
    }

    public static void backTrackingNumber(int s, int length, int n){
        s=1000000;
        if (length==n){
            System.out.println(s);
        } else if (length==0) {
            for (int i=0;i<10;i++){
                backTrackingNumber(i,length,n);
            }
        } else {
            boolean pr = true;
            for (int i=0;i<10;i++){
                int x = s*10+i;
                if(good(x)){
                    backTrackingNumber(x,length+1,n);
                }else{
                    pr=false;
                }
            }
        }
    }
    public static boolean good(int x){
        long sum =0;
        while (x>0){
            sum+=sum%10;
            x=x/10;
        }
        return sum<=13;
    }

    public static void main(String[] args) {
        backTrackingNumber(0,0,7);
    }
}