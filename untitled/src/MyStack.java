/**
 * Created by salavatshirgaleev on 10.03.15.
 */
public interface MyStack<T> {
    void push(T x);
    T pop();
    boolean isEmpty();

}
