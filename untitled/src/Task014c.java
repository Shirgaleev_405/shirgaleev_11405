import java.util.Scanner;
import java.util.Stack;
/**
 * Created by salavatshirgaleev on 10.03.15.
 */
public class Task014c extends MyLinkedStack<Integer> {
    public static void main(String[] args) {
        Stack s=new Stack();
        Scanner scanner= new Scanner(System.in);
        final int CAPACITY = 999;
        int a[] = new int[CAPACITY];
        for (int i=0;i< CAPACITY;i++){
            a[i]= scanner.nextInt();

        }
        for (int i=0;i< CAPACITY;i++){
            s.push(a[i]);

        }
        for (int i=0;i<CAPACITY;i++){
            System.out.println(s.pop());
        }
    }


}
