import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 12.02.15.
 */
public class Task002c {
    static double level(int n, double x) {
        if (n == 0)
            return 1;
        else
            return x * level(n - 1, x);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();
        int n = scanner.nextInt();
        System.out.println(level(n, x));
    }
}

