import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 24.02.15.
 */
public class Task010a {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        ElemP head = null;
        ElemP p = null;
        int e = 1;
        int s = 0;
        for (int i = 0; i < n; i++) {
            p = new ElemP(scanner.nextInt(), head);
            head = p;
        }
        p = head;
        while (p != null) {
            s = s + p.getValue() * e;
            e = e * 2;
            p.getNext();
        }
        for (int i = 0; i < n; i++) {
            p = new ElemP(s % 10, head);
            head = p;
            s=s/10;
        }

        while (p!=null){
            System.out.println(p.getValue());
            p.getNext();
        }
    }
}
