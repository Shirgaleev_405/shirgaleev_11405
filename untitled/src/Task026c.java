import java.io.*;
import java.nio.Buffer;

/**
 * Created by salavatshirgaleev on 10/05/15.
 */
public class Task026c {
    public static void main(String[] args) throws IOException {
        FileReader reader = new FileReader("Original.txt");
        FileWriter writer = new FileWriter("Original.txt");
        int n = reader.read();
        long a = System.nanoTime();
        while (n != -1) {
            writer.write(n);
            n = reader.read();
        }
        long b = System.nanoTime();
        long result1 = b - a;
        reader.close();
        writer.close();


        BufferedReader br = new BufferedReader(new FileReader("Original.txt"));
        PrintWriter pw = new PrintWriter(new FileWriter("Copy.txt"));
        String read;
        long c = System.nanoTime();
        while ((read = br.readLine()) != null) {
            pw.write(read);
        }
        long d=System.nanoTime();
        long result=d-c;
        br.close();
        pw.close();
        System.out.print("Разница во времени:");
        System.out.println(result-result1);
    }
}
