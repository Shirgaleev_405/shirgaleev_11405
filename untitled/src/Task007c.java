import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 19.02.15.
 */
public class Task007c {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        ElemP head = null;
        ElemP p = null;

        for (int i = 0; i < n; i++) {
            p = new ElemP(scanner.nextInt(), head);
            head = p;
        }
        p = head;
        while (p!=null){
            if(p.getNext().getNext().getNext()==null){
                p.setNext(null);
            }
            p.getNext();
        }
    }


}
