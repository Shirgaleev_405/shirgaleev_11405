import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 23.03.15.
 */
public class LeksGraphComparator implements Comparator<Vector2D> {
    public int compare (Vector2D o1, Vector2D o2){
        if (!o1.equals(o2)) {
            if (o1.getX() != o2.getX()) {
                if (o1.getX() > o2.getX()) return 1;
                else return -1;
            }
            if (o1.getY() > o2.getY()) return 1;
            else return -1;
        }
        return 0;
    }

    public static void main(String[] args) {
        ArrayList<Vector2D> list = new ArrayList<Vector2D>();
        list.add(new Vector2D(1, 3));
        list.add(new Vector2D(1, 0));
        list.add(new Vector2D(5, 7));
        list.add(new Vector2D(8, 0));
        list.add(new Vector2D(0, 0));
        list.add(new Vector2D(3,2));
        Collections.sort(list, new LeksGraphComparator());
        System.out.println(list);
    }
}


