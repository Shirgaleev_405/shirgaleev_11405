import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;


/**
 * Created by salavatshirgaleev on 10.03.15.
 */
public class MyCollection<T> implements Collection<T>{
    protected final int CAPACITY = 123;
    Scanner scanner = new Scanner(System.in);
    int size = scanner.nextInt();
    ElemP head = null;
    ElemP p = null;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(p.getValue())) return true;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        return false;
    }


    @Override
    public boolean add(ElemP integer) {
        if (size != CAPACITY) {
            ElemP p = head;
            p.getValue() = integer;
            size++;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean remove(Object o) {
        if (o.equals(head.value)) {
            head = head.next;
            size--;
            return true;
        }
        ElemP p = head;
        while (p.getNext().getNext() != null) {
            if (o.equals(p.getNext().getValue())) {
                p.setNext(p.getNext().getNext());
                size--;
                return true;
            }
            p = p.getNext();
        }
        if (o.equals(p.getNext().getValue())) {
            p.setNext(null);
            size--;
            return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }


    @Override
    public boolean containsAll(Collection<T> c) {
        for (Object x : c) {
            if (!contains(x)) return false;
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<T> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<ElemP>  c) {
        return false;
    }




    @Override
    public boolean removeAll(Collection<T> c) {
        int s = size;
        for (Object x : c) {
            this.remove(x);
        }
        if (s != this.size) return true;
        return false;
    }

    @Override
    public boolean retainAll(Collection<T> c) {
        int s = size;
        for (int i = 0; i < size; i++) {
            if (!c.contains(p.getValue())) {
                this.remove(p.getValue());
                i--;
            }
        }
        if (s != this.size) return true;
        return false;
    }

    @Override
    public void clear() {
        for (int i = 0; i < size(); i++) {
            remove(p);
        }
        size = 0;
    }
}
