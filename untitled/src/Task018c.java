import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by salavatshirgaleev on 23.03.15.
 */
public class Task018c {
    public static void main(String[] args) {
        List<Vector2D> list = new ArrayList<Vector2D>();
        Vector2D a=new Vector2D(3,1);
        Vector2D b=new Vector2D(0,1);
        Vector2D c=new Vector2D(4,0);
        Vector2D d=new Vector2D(2,2);
        Vector2D e=new Vector2D(1,2);
        Vector2D g=new Vector2D(2,2);
        list.add(a);
        list.add(b);
        list.add(c);
        list.add(d);
        list.add(e);
        list.add(g);
        Collections.sort(list);
    }

}
