/**
 * Created by salavatshirgaleev on 09.02.15.
 */
public class Elem {
    int value;
    ElemP next;

    public Elem(int value, ElemP next) {
        this.value = value;
        this.next = next;
    }

    public Elem() {
    }

    public ElemP getNext() {
        return next;
    }

    public void setNext(ElemP next) {
        this.next = next;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}