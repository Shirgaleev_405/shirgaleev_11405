import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Scanner;


/**
 * Created by salavatshirgaleev on 12/05/15.
 */
public class Task028b extends Thread {
    private int[] mas;
    static int sum = 0;
    private int begin;
    private int end;


    public Task028b(int[] mas, int begin, int end) {
        this.mas = mas;
        this.begin = begin;
        this.end = end;
    }

    @Override
    public void run() {
        int sum = 0;
        for (int i = begin; i < end; i++) {
            sum = sum + mas[i];
        }
        System.out.println(sum);
    }

    public static void main(String[] args) throws InterruptedException {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int[] mas = new int[n];
        int y;
        int p;
        for (int i = 0; i < n; i++) {
            mas[i] = i;
        }
        y = n / k;
        ArrayList<Task028b> m = new ArrayList<t>();
        int start = 0;
        int t = y;
        for (int i = 0; i < k - 1; i++) {
            Task028b r = new Task028b(mas, start, y);
            start = y;
            y = y + t;
            m.add(r);
        }
        Task028b r = new Task028b(mas, start, mas.length);
        m.add(r);
        for (int i = 0; i < m.size(); i++) {
            m.get(i).start();
            m.get(i).join();
        }
    }
}
