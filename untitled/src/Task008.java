import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 12.02.15.
 */
public class Task008 {
    static String Table(int k, int n) {
        System.out.println(k + " * " + n + " = " + k * n);
        if (n == 9) {
            return k + " * " + 9 + " = " + k * 9;
        } else
            return Table(k, n + 1);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        Table(k, 2);
    }
}
