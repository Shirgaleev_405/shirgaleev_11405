import java.util.Stack;
/**
 * Created by salavatshirgaleev on 20/04/15.
 */
public class Task020a {
    static void LPK(Node n){
        int i = 0;
        Stack<Node> stack = new Stack<Node>();
        Node p;
        stack.push(n);
        do {
            p = stack.pop();
            while (p != null && p.getColor() == 0) {
                stack.push(p);
                p = p.getLeft();
            }
            p = stack.pop();
            if (p.getRight() != null && p.getRight().getColor() == 0){
                stack.push(p);
                stack.push(p.getRight());
            }
            else {
                if (p.getColor() == 0) {
                    p.setValue(i);
                    i++;
                    p.setColor(1);
                }
            }




        } while (!stack.isEmpty());
    }
}
