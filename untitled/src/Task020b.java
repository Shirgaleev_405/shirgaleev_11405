import java.util.*;
import java.util.Stack;

/**
 * Created by salavatshirgaleev on 20/04/15.
 */
public class Task020b {
    static void LKP(Node p){
        Stack<Node> stack = new Stack<Node> ();
        while (p!=null || !stack.empty()){
            if (!stack.empty()){
                p=stack.pop();
                if (p.getRight()!=null) p=p.getRight();
                else p=null;
            }
            while (p!=null){
                stack.push(p);
                p=p.getLeft();
            }
        }
    }
}
