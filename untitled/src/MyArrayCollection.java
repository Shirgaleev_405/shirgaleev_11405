import java.util.Collection;
import java.util.Iterator;

/**
 * Created by salavatshirgaleev on 10.03.15.
 */
public class MyArrayCollection implements Collection<Integer> {
    protected final int CAPACITY = 123;
    protected int[] arr = new int[CAPACITY];
    protected int size = 0;

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean contains(Object o) {
        for (int x : arr) {
            if (o.equals(x)) return true;
        }
        return false;
    }


    public boolean add(Integer integer) {
        if (size != CAPACITY) {
            arr[size] = integer;
            size++;
            return true;
        }
        return false;
    }

    public boolean containsAll(Collection<?> c) {
        for (Object x : c) {
            if (!contains(x)) return false;
        }
        return true;
    }

    public boolean addAll(Collection<? extends Integer> c) {
        if (this.size() + c.size() > CAPACITY) return false;
        for (int x : c) {
            this.add(x);
        }
        return true;
    }

    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(arr[i])) {
                while (i < size - 1) {
                    arr[i] = arr[i + 1];
                    i++;
                }
                size--;
                return true;
            }
        }
        return false;
    }

    public boolean removeAll(Collection<?> c) {
        int s = size;
        for (Object x : c) {
            this.remove(x);
        }
        if (s != this.size) return true;
        return false;
    }

    public boolean retainAll(Collection<?> c) {
        int s = size;
        for (int i = 0; i < size; i++) {
            if (!c.contains(arr[i])) {
                this.remove(arr[i]);
                i--;
            }
        }
        if (s != this.size) return true;
        return false;
    }

    public void clear() {
        for (int i = 0; i < size(); i++) {
            remove(arr[i]);
        }
        size = 0;
    }

    @Override
    public Iterator<Integer> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

}

