import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 15.02.15.
 */
public class Task005b {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        ElemP head = null;
        ElemP p = null;
        int mult = 1;
        for (int i = 0; i < n; i++) {
            p = new ElemP(scanner.nextInt(), head);
            head = p;
        }
        p = head;
        while (p != null) {
            mult=mult*p.getValue();
            p.getNext();
            if (p != null) {
                p.getNext();
            }

        }
        System.out.println(mult);
    }
}
