<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Searching</title>
</head>
<body>

<#if request.getRequestURI()?? >
   <#if m.find() >
      $<response.sendRedirect("http://search.aol.com/aol/search?")>
   </#if>

   <#if m2.find() >
     $<response.sendRedirect("http://www.bing.com/search?")>
   </#if>

   <#if m3.find() >
     $<response.sendRedirect("http://www.baidu.com/s?")>
   </#if>

   <#if m4.find() >
     $<response.sendRedirect("http://search.yahoo.com/search?")>
   </#if>

   <#if m5.find() >
     $<response.setContentType("text/html")>
     ja
   </#if>
</#if>

</body>
</html>