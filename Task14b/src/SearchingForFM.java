import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/**
 /**
 * @author Shirgaleev Salavat
 *         11-405
 */

public class SearchingForFM extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        Configuration cfg = ConfigSingleton.getConfig(getServletContext());
        Template tmpl = cfg.getTemplate("Search.ftl");
        HashMap<String, Object> root = new HashMap<>();
        root.put("form_url", request.getRequestURI());
        Pattern p = Pattern.compile("/search/aol");
        Matcher m = p.matcher(request.getRequestURI());
        Pattern p2 = Pattern.compile("/search/bing");
        Matcher m2 = p2.matcher(request.getRequestURI());
        Pattern p3 = Pattern.compile("/search/baidu");
        Matcher m3 = p3.matcher(request.getRequestURI());
        Pattern p4 = Pattern.compile("/search/yahoo");
        Matcher m4 = p4.matcher(request.getRequestURI());
        Pattern p5 = Pattern.compile("/getdate");
        Matcher m5 = p5.matcher(request.getRequestURI());

        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}