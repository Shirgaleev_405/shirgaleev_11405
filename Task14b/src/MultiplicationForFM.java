import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Shirgaleev Salavat
 *         11-405
 */

public class MultiplicationForFM extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        Configuration cfg = ConfigSingleton.getConfig(getServletContext());
        Template tmpl = cfg.getTemplate("Mult.ftl");
        Pattern p = Pattern.compile("/mult/([0-9]+)/([0-9]+)");
        Matcher m = p.matcher(request.getRequestURI());
        int a = new Integer(m.group(1));
        int b = new Integer(m.group(2));

        try {
            tmpl.process((a * b), response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
