import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

/**
 * Created by salavatshirgaleev on 12.03.15.
 */

class YearComparator implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        return o1.getYear()-o2.getYear();
    }
}
class GroupComparator implements Comparator<Student>{

    @Override
    public int compare(Student o1, Student o2) {
        return o1.getGroup().compareTo(o2.getGroup());
    }
}

public class Student  implements Comparable<Student> {
    private String lastName;
    private int year;
    private String group;

    public Student(String d, int b, String c) {
        lastName = d;
        year = b;
        group = c;
    }

    public String toStrinf() {
        return lastName + year + group;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }


    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }


    @Override
    public int compareTo(Student o) {
        return lastName.compareTo(o.getLastName());
    }

    public static void main(String[] args) {
        ArrayList<Student> d = new ArrayList<Student>();
        Iterator<Student> i = d.iterator();
        while (i.hasNext()){
            System.out.println(i.next());
        }
        d.add(new Student("Gilyazova", 1996, "11-405"));
        d.add(new Student("Nurgaliev", 1995, "11-405"));
        d.add(new Student("Tataskih", 1996, "11-401"));
        d.add(new Student("Mironov", 1995, "11-401"));
        System.out.println(d);
        Collections.sort(d);
        System.out.print(d);
        Collections.sort(d,new GroupComparator());
        System.out.println(d);
    }



}
