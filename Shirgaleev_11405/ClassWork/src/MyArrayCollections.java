import java.util.Collection;

/**
 * Created by salavatshirgaleev on 19.02.15.
 */

public abstract class MyArrayCollection implements Collection<Integer> {
    protected final int CAPACITY = 999;
    protected int a[] = new int[CAPACITY];
    protected int n = 0;

    public boolean add(int x) {
        if (n == CAPACITY) {
            return false;
        } else {
            a[n] = x;
            n++;
            return true;
        }
    }

    @Override
    public int size() {
        if (n == 0) {
            return 0;
        } else {
            return n;
        }
    }

    @Override
    public boolean isEmpty() {
        if (n == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 1; i < CAPACITY; i++) {
            if (a[i] == o) {
                return true;
            } else {
                return false;
            }

        }
    }

    @Override
    public Object[] toArray() {
        for (int i = 0; i < CAPACITY; i++) {
            int[] m = new int[CAPACITY];
            a[i] = m[i];
        }


    }

    @Override
    public boolean remove(Object o) {

        for (int i = 1; i < CAPACITY; i++) {
            if (a[i] == o) {
                for (int j = i; j < CAPACITY; j++, i++) {
                    a[i] = a[j];
                    a[CAPACITY] = 0;
                }
            }
        }
    }

    @Override
    public boolean containsAll(Object o) {
        for (int i = 1; i < CAPACITY; i++) {
            if (a[i] == o) {
                n++;
            }
        }
        if (n == CAPACITY) {
            return true;
        } else {
            return false;
        }


    }

    @Override
    public boolean addAll(Collection<? extends Integer> collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public void clear() {
    }
}
}