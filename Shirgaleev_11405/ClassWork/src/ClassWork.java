import java.util.PriorityQueue;

/**
 * Created by salavatshirgaleev on 13.03.15.
 */
public class ClassWork {
    public static void main(String[] args) {
        PriorityQueue<Student> pq=new PriorityQueue<Student>();
        pq.offer(new Student("Gilyazova", 1996, "11-405"));
        pq.offer(new Student("Nurgaliev", 1995, "11-405"));
        pq.offer(new Student("Mironov", 1995, "11-401"));
        pq.offer(new "Tataskih", 1996, "11-401"));
        while (!pq.isEmpty()){
            System.out.println(pq.poll());
        }
    }
}
