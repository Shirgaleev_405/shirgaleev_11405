/**
 * Created by salavatshirgaleev on 05.03.15.
 */
public class MyLinkedQueue<T> implements MyQueue<Elem> {
    private Elem head;
    private Elem tail;

    @Override
    public void offer(Elem a) {
        Elem p = new Elem(a, null);
        if (head == null) {
            head = p;
            tail = p;
        } else {
            tail.setNext(p);
            tail = p;
        }
    }

    @Override
    public Elem peek() {
        if(head!=null){
            return head;
        }else{
            return null;
        }

    }

    @Override
    public Elem poll() {
        if(head!=null){
            return head;
        }else{
            return null;
        }
    }

    @Override
    public boolean isEmpty() {
        if (head!=null){
            return false;
        } else {
            return true;
        }
    }


}
