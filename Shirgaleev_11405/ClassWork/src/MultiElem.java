import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 13.02.15.
 */
public class MultiElem {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        Elem head = null;
        Elem p;
        int mult = 1;
        for (int i=0;i<n;i++){
            p=new Elem();
            p.set(scanner.nextInt());
            p.setNext(head);
            head=p;
            mult=mult*p.getValue();
        }
    }
}
