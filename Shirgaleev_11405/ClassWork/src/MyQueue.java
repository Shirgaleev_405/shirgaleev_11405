/**
 * Created by salavatshirgaleev on 05.03.15.
 */
public interface MyQueue <T> {
    void offer(T s);
    T peek();
    T poll();
    boolean isEmpty();

}
