/**
 * Created by salavatshirgaleev on 02.03.15.
 */
public interface Stack<T> {
    void push(T x);
    T pop();
    boolean isEmpty();

}
