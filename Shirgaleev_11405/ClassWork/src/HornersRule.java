import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 13.02.15.
 */
public class HornersRule {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int x = scanner.nextInt();
        Elem head = null;
        Elem p;
        int s = 0;
        for (int i = 0; i < n; i++) {
            p = new Elem();
            p.set(scanner.nextInt());
            p.setNext(head);
            head = p;
        }
        head = null;
        while (p!=null ) {
            s = s * x + p.getValue();
        }
        System.out.println(s);
    }
}
