/**
 * Created by salavatshirgaleev on 09.02.15.
 */
public class Elem<T> {

    private Elem<T> next;
    private T value;

    public Elem(T value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public Elem() {
    }

    public Elem<T> getNext() {
        return next;
    }

    public void setNext(Elem next) {
        this.next = next;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }




}