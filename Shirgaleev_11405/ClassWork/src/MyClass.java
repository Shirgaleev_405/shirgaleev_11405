/**
 * Created by salavatshirgaleev on 09.02.15.
 */
public class MyClass {
    public static void main(String[] args) {
        Elem head = null;
        Elem p;

        p = new Elem(2, head);
        head = p;

        p = new Elem(5, head);
        head = p;

        p = head;
        int sum = 0;

        while (p != null) {
            System.out.println(p.getValue());

            p = p.getNext();
        }
        System.out.println(sum);


    }


}
