import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 15.02.15.
 */
public class Task005c {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        ElemP head = null;
        ElemP p = null;
        int sum = 0;
        for (int i = 0; i < n; i++) {
            p = new ElemP(scanner.nextInt(), head);
            head = p;
        }
        p = head;
        while (p != null) {
            if ((p.getValue() / 2 )== 0) {
                sum=sum+p.getValue();
            }
            p.getNext();
        }
        System.out.println(sum);
    }
}
