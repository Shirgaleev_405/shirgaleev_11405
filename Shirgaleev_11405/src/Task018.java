import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 12.02.15.
 */
public class Task018 {
    static Scanner scanner = new Scanner(System.in);

    static double Polynomial(double x, int n, double a) {
        if (n == 0) {
            return a;
        } else
            a = a * x + scanner.nextDouble();
        return Polynomial(x, n - 1, a);
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();
        int n = scanner.nextInt();
        double p = scanner.nextDouble();
        double a = p * x;
        p = scanner.nextDouble();
        a = a + p;
        System.out.println(Polynomial(x, n - 1, a));
    }
}

