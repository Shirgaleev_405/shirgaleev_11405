import java.util.*;
import java.util.Stack;

/**
 * Created by salavatshirgaleev on 16/04/15.
 */
public class Task020c {
    public static int wide(Tree t) {
        Queue<Node> q = new LinkedList<Node>();
        int sum = 0;
        q.offer(t.getRoot());
        while (!q.isEmpty()) {
            Node p = q.poll();
            sum = sum + p.getValue();
            if (p.getLeft() != null) {
                q.offer(p.getLeft());
            }
            if (p.getLeft() != null) {
                q.offer(p.getRight());
            }
        }

        return sum;
    }

    static void maxKLP(Node p) {
        Stack<Node> stack = new Stack<Node>();
        int max=p.getValue();
        while (p != null || !stack.empty()) {
            if (!stack.empty()) {
                p = stack.pop();
            }
            while (p != null) {
                if (p.getValue()>max){
                    max=p.getValue();
                }
                if (p.getRight() != null) stack.push(p.getRight());
                p = p.getLeft();
            }
        }
    }



    public static int mult(Node p) {
        int multvalue = mult(p);
        if (p.getLeft() != null) mult(p.getLeft());
        if (p.getRight() != null) mult(p.getRight());
        return multvalue;
    }
}