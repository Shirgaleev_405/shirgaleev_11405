import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 12.02.15.
 */
public class Task013 {
    static double Wallis(double n) {
        if (n == 1) {
            return (float) 4 / 3;
        } else
            return ((4 * n * n) / (4 * n * n - 1)) * Wallis(n - 1);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(Wallis(n));
    }
}
