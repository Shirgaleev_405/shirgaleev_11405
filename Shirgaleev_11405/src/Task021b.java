import java.util.*;
import java.util.Stack;

/**
 * Created by salavatshirgaleev on 20/04/15.
 */
public class Task021b {
    Node root;


    public int size() {
        Node p = root;
        Queue<Node> queue = new LinkedList<Node>();
        int size = 0;
        do {
            size++;
            if (p.getLeft() != null) queue.add(p.getLeft());
            if (p.getRight() != null) queue.add(p.getRight());
            if (!queue.isEmpty()) p = queue.poll();
        } while (!queue.isEmpty());
        return size;

    }

    public boolean isEmpty() {
        return root == null;
    }

    public boolean contains(Object o) {
        Node p = root;
        do {
            if (o.equals(p.getValue())) return true;
            if (((Integer) o) > p.getValue()) p = p.getRight();
            else p = p.getLeft();
        } while (p != null);
        return false;

    }


    public Object[] toArray() {
        Object[] array = new Object[size()];
        int i = 0;
        Stack<Node> stack = new Stack<Node>();
        Node p;
        stack.push(root);
        do {
            p = stack.pop();
            while (p != null) {
                stack.push(p);
                p = p.getLeft();
            }
            do {
                if (stack.isEmpty()) break;
                p = stack.pop();
                array[i] = p.getValue();
                i++;
            } while (!stack.isEmpty() && p.getRight() == null);
            if (p.getRight() != null) stack.push(p.getRight());
        } while (!stack.isEmpty());

        return array;
    }


    public <T> T[] toArray(T[] a) {
        if (a.length < this.size()) {
            return (T[]) Arrays.copyOf(this.toArray(), this.size(), a.getClass());
        }
        System.arraycopy(this.toArray(), 0, a, 0, this.size());
        if (a.length > this.size()) {
            a[this.size()] = null;
        }
        return a;
    }


    public boolean add(Integer integer) {
        Node p = root, l;
        do {
            l = p;
            if (p.getValue() == integer) return false;
            else if (integer < p.getValue()) p = p.getLeft();
            else p = p.getRight();

        } while (p != null);
        if (integer < l.getValue()) l.setLeft(new Node(integer));
        else l.setRight(new Node(integer));
        return true;
    }


    public boolean remove(Object o) {
        int i = 0;
        Node p = root, l;
        do {
            l = p;
            if (o.equals(p.getValue())) {
                i = 1;
                break;
            }
            if (((Integer) o) > p.getValue()) p = p.getRight();
            else p = p.getLeft();
        } while (p != null);
        if (i == 0) return false;
        if (p.getLeft() == null && p.getRight() == null) {
            p = null;
            return true;
        }
        if (p.getRight() == null && p.getLeft() != null) {
            if (p.getValue() > l.getValue()) l.setRight(p.getRight());
            else l.setLeft(p.getRight());
            return true;
        }
        if (p.getRight() != null && p.getLeft() == null) {
            if (p.getValue() > l.getValue()) l.setRight(p.getLeft());
            else l.setLeft(p.getLeft());
            return true;
        }
        if (p.getRight() != null && p.getLeft() != null) {
            Node min = min(p.getRight());
            p.setValue(min.getValue());
            remove(min);
            return  true;

        }


        return true;
    }

    public Node min(Node node) {
        Node p = node;
        while (p.getLeft() != null) {
            p = p.getLeft();
        }
        return p;
    }

    public boolean containsAll(Collection<?> c) {
        int i = 0;
        for (Object a : c) {
            if (contains(a) != true) {
                i = 1;
                break;
            }

        }
        return i == 0;
    }


    public boolean addAll(Collection<? extends Integer> c) {
        int i = 0;
        for (int a : c) {
            if (add(a) != true) {
                i = 1;
                break;
            }
        }
        return i == 0;
    }


    public boolean retainAll(Collection<?> c) {
        Node p = root;
        int i = 0;
        Queue<Node> queue = new LinkedList<Node>();

        do {
            if (!c.contains(p)) {
                i++;
                remove(p);
            }
            if (p.getLeft() != null) queue.add(p.getLeft());
            if (p.getRight() != null) queue.add(p.getRight());
            if (!queue.isEmpty()) p = queue.poll();
        } while (!queue.isEmpty());

        return i!=0;
    }


    public boolean removeAll(Collection<?> c) {
        int i = 0;
        for (Object a : c) {
            if (remove(a) != true) {
                i = 1;
                break;
            }
        }
        return i == 0;
    }


    public void clear() {
        root = null;

    }


    public Iterator<Integer> iterator() {
        return null;
    }


}
