/**
 * Created by salavatshirgaleev on 10.03.15.
 */
public abstract class MyLinkedStack<T> implements MyStack<T> {
    ElemP head=null;
    private T x;

    public MyLinkedStack() {
        head = null;
    }

    public void push(T x) {
        ElemP<T> p = new ElemP(x, head);
        head = p;
    }

    public T pop() {
        if (head == null) {
            return null;
        }
        Elem p = head.getValue();
        head = head.getNext();
        return p;
    }

    public boolean isEmpty() {
        return head == null;
    }
}
