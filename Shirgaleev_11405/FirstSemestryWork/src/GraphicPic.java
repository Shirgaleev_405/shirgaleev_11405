import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 26.02.15.
 */
public class GraphicPic extends LinkedList {
    public Figure getHead() {
        return head;
    }

    public void setHead(Figure head) {
        this.head = head;
    }

    private Figure head;

    public Figure getTail() {
        return tail;
    }

    public void setTail(Figure tail) {
        this.tail = tail;
    }

    private Figure tail;


    private int f, x1, y1, x2, y2, colour;

    public GraphicPic hasSquareBiggerThanS(double s) {
        GraphicPic m = (GraphicPic) new LinkedList();
        Figure j = new Figure(f, x1, y1, x2, y1, colour);
        if (j.square() > s) {
            if (m.head == null) {
                m.head = j;
                m.tail = j;
            } else {
                j.next = m.head;
                m.head = j;
            }
        }
        return m;
    }

    public void show(Figure p) throws FileNotFoundException {
        Scanner scan = new Scanner(new File("text.txt"));
        while (scan.nextLine() != null) {
            if (scan.nextInt() == 1) {
                System.out.println("Прямоугольник");
            }
            if (scan.nextInt() == 2) {
                System.out.println("Отрезок");
            }
            if (scan.nextInt() == 3) {
                System.out.println("Круг");
            }
        }
    }

    public void delete(int i) {
        Figure f = head;
        while (f.getNext() != null) {
            if (f.getNext().getF() == i) {
                f.setNext(f.getNext().getNext());
            } else f.getNext();
        }
    }

    public void insert(Figure g) {
        Figure f = head;
        while (f.getNext() != null) {
            if ((g.getX1() == f.getX1()) && (g.getX2() == f.getX2()) && (g.getY1() == f.getY1() && (g.getY2() == f.getY2()))) {
                f.setColour(f.getColour());
            } else {
                f.setNext(g);
                g.setNext(f.getNext().getNext());
            }
        }
    }

    public GraphicPic commonWith(Figure r) {
        GraphicPic m = (GraphicPic) new LinkedList();
        Figure j = new Figure(f, x1, y1, x2, y1, colour);
        if (j.getX1() < r.getX1() || (j.getX1() > r.getX2()) || (j.getY2() < r.getY1() || (j.getY1() > r.getY2()))) {
            if (m.head == null) {
                m.head = j;
                m.tail = j;
            } else {
                j.next = m.head;
                m.head = j;
            }
        }
        return m;
    }

    public GraphicPic(String Text) throws FileNotFoundException {
        Scanner scan = new Scanner(new File("text.txt"));
        Figure j = new Figure(f, x1, y1, x2, y2, colour);
        int r = 0;
        while (scan.hasNext())
            if (scan.nextLine() == null) {
                r++;
            }
        while (scan.nextLine() != null) {
            Figure head = null;
            for (int i = 0; i < r; i++) {
                j = new Figure(j.getF(), j.getX1(), j.getY1(), j.getX2(), j.getY2(), j.getColour());
                j.setNext(head);
                head = j;
            }
        }
    }

}
