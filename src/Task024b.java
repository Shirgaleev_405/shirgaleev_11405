import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 11/05/15.
 */
public class Task024b {
    public static void backTracking(int s, int m, int k) {
        if (numberD(s) == m) {
            System.out.println(s);
        } else {
            if (numberD(s) == 0) {
                for (int i = 0; i < 9; i++) {
                    String r = "" + i;
                    backTracking(stringToInt(r), m, k);
                }
            } else {
                boolean f = true;
                for (int i = 0; i < 9 && f; i++) {
                    String r = intToString(s) + i;
                    if (passConditional(r, m, k)) {
                        backTracking(stringToInt(r), m, k);
                    } else {
                        f = false;
                    }
                }
            }
        }
    }

    private static boolean passConditional(String r, int m, int k) {
        int n = stringToInt(r);
        int a = 0, b = 0;
        while (n > 0) {
            b = n % 10;
            a++;
            n = n / 10;
        }
        if (a == m && b < k) {
            return true;
        } else {
            return false;
        }
    }

    public static int numberD(int s) {
        int i = 0;
        while (s > 0) {
            s = s / 10;
            i++;
        }
        return i;
    }

    public static int stringToInt(String r) {
        try {
            Integer i1 = new Integer(r);
            return i1;
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
            return 0;
        }
    }

    public static String intToString(int r) {
        String str = Integer.toString(r);
        return str;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int s = scanner.nextInt();
        int m = scanner.nextInt();
        int k = scanner.nextInt();
        backTracking(1234, 4, 6);
    }
}
