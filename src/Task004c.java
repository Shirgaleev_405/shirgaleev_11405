import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 14.02.15.
 */
public class Task004c{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        ElemP head = null;
        ElemP p = null;
        boolean pr = false;
        for (int i = 0; i < n; i++) {
            p = new ElemP(scanner.nextInt(), head);
            head=p;
        }
        p=head;
        while (p!=null && !pr) {
            pr=(p.getValue()==0);
            p.getNext();
        }
        System.out.println(pr);
    }
}
