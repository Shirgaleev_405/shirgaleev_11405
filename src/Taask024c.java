/**
 * Created by salavatshirgaleev on 11/05/15.
 */
public class Taask024c {
    public static void backTracking(String s, int n) {
        if (s.length() == n) {
            System.out.println(s);
        } else {
            if (s.length() == 0) {
                for (int i = 97; i < 123; i++) {
                    String r = "" + (char) i;
                    backTracking(r, n);
                }
            } else {
                boolean f = true;
                for (int i = 97; i < 123 && f; i++) {
                    String r = s + (char) i;
                    if (passConditional(r)) {
                        backTracking(r, n);
                    } else {
                        f = false;
                    }
                }
            }
        }
    }

    private static boolean passConditional(String r) {
        int n = 0;
        String[] z = r.split("");
        for (String l : z) {
            if (numberVowel(l)) {
                n++;
            }
            if (n > 3) {
                break;
            }
        }
        return (n < 4);
    }

    private static boolean numberVowel(String l) {
        return l.equals("a") || l.equals("e") || l.equals("o") || l.equals("u") || l.equals("y") || l.equals("i");
    }

    public static void main(String[] args) {
        backTracking("",3);
    }
}
