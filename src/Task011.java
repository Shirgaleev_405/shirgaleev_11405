import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 12.02.15.
 */
public class Task011 {
    static int Factorial(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else
            return n * Factorial(n - 2);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println(Factorial(n));
    }
}

