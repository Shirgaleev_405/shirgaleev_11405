import java.util.Scanner;

/**
 * Created by salavatshirgaleev on 15.02.15.
 */
public class Task005a {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        ElemP head = null;
        ElemP p = null;
        int min =0 ;
        int max=0;
        for (int i = 0; i < n; i++) {
            p = new ElemP(scanner.nextInt(), head);
            head = p;
        }
        p = head;
        while (p != null){
            if ((p.getValue()>p.getNext().getValue())&&(p.getNext().getValue()<p.getNext().getValue())){
                min++;
            }
            if((p.getValue()>p.getNext().getValue())&&(p.getNext().getValue()<p.getNext().getValue())){
                max++;
            }
            if (p.getNext().getNext().getNext()!=null) {
                p.getNext();
            }
        }
        System.out.println("Min local =" +min );
        System.out.println("Max local =" +max );
    }
}