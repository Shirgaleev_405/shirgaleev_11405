<%--
  Created by IntelliJ IDEA.
  User: salavatshirgaleev
  Date: 12.11.15
  Time: 17:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Природа</title>
    <link href="/CSS/style.css" rel="stylesheet" type="text/css"/>
    <link href="/CSS/bootstrap.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/npm.js"></script>
</head>

<body>
<ul id="navbar">
    <li><a href="/index">Главная</a></li>
    <li><a href="/fotoalbom" class="brd">Фотоальбом</a></li>
    <li><a href="/nature">Природа</a></li>
    <li><a href="/adventure">Приключения</a></li>
    <li><a href="/spravka">Справочные данные</a></li>
    <li><a href="/time">Часовые пояса</a></li>
    <li><a href="/interactive">Интерактивные задания</a></li>
    <li><a href="/travelling">Путешествия</a></li>
    <li><a href="/science" class="brd">Наука</a></li>

</ul>


<div class="priroda">
    <div style="text-align: center;"><img src="priroda/tykva.jpg" border="5" bordercolor="#008000 " width="400"
                                          height="400"></div>
    <h1>893-килограммовый гигант выиграл тыквенный конкурс</h1>

    <div>В Калифорнии на протяжении 45 лет проходит фестиваль Half Moon Bay Pumpkin. Помимо множества развлекательных
        мероприятий, связанных с праздником урожая, здесь устраивается конкурс на самую гигантскую тыкву. Победитель
        этого
        года весит 893 килограмма.
    </div>
</div>

<div class="priroda">
    <div style="text-align: center;"><img src="priroda/tree.jpg" border="5" bordercolor="#008000 " width="400"
                                          height="400"></div>
    <h1>Деревья Нью-Йорка получат адреса электронной почты</h1>

    <div>Две сотни деревьев Нью-Йорка получат собственные адреса электронной почты. Нововведение позволит оперативно
        информировать власти об экологических проблемах.
    </div>
</div>


<div class="priroda">
    <div style="text-align: center;"><img src="priroda/bars.jpg" border="5" bordercolor="#008000 " width="400"
                                          height="400"></div>
    <h1>На Алтае задержан браконьер со шкурой и черепом снежного барса</h1>

    <div>Части краснокнижного животного были обнаружены в машине жителя Республики Алтай. Полицией Барнаула заведено
        уголовное дело, ведется расследование.
    </div>
</div>

</body>
</html>
