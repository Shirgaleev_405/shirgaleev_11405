<%--
  Created by IntelliJ IDEA.
  User: salavatshirgaleev
  Date: 12.11.15
  Time: 17:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Фотоальбом</title>


    <link href="/CSS/style.css" rel="stylesheet" type="text/css"/>
    <link href="/CSS/bootstrap.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/jquery.js"></script>
    <script src="/js/bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="/js/npm.js"></script>


</head>

<body>
<ul id="navbar">
    <li><a href="/index">Главная</a></li>
    <li><a href="/fotoalbom">Фотоальбом</a></li>
    <li><a href="/nature">Природа</a></li>
    <li><a href="/adventure">Приключения</a></li>
    <li><a href="/spravka">Справочные данные</a></li>
    <li><a href="/time">Часовые пояса</a></li>
    <li><a href="/interactive">Интерактивные задания</a></li>
    <li><a href="/travelling">Путешествия</a></li>
    <li><a href="/science">Наука</a></li>

</ul>


<div class="allphoto" align="center" id="pho">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">

            <div class="item active">
                <img src="albom/yellowstone.jpg" width="1280" height="720">
            </div>

            <div class="item">
                <img src="albom/toratay.jpg" width="1280" height="720">
            </div>

            <div class="item">
                <img src="albom/colorado.jpg" width="1280" height="720">
            </div>

            <div class="item">
                <img src="albom/baykal.jpg" width="1280" height="720">
            </div>

            <div class="item">
                <img src="albom/grandcanyon.jpg" width="1280" height="720">
            </div>
        </div>


        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>


</body>
</html>
