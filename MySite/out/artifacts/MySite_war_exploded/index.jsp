<%--
  Created by IntelliJ IDEA.
  User: salavatshirgaleev
  Date: 12.11.15
  Time: 17:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Главная страница</title>
  <link href="/CSS/style.css" rel="stylesheet" type="text/css"/>
  <link href="/CSS/bootstrap.css" rel="stylesheet" type="text/css"/>
  <script type="text/javascript" src="/js/bootstrap.min.js"></script>
  <script src="/js/bootstrap.js" type="text/javascript"></script>
  <script type="text/javascript" src="/js/npm.js"></script>
</head>

<body>
<ul id="navbar">
  <li><a href="/index">Главная</a></li>
  <li><a href="/fotoalbom" class="brd">Фотоальбом</a></li>
  <li><a href="/nature">Природа</a></li>
  <li><a href="/adventure">Приключения</a></li>
  <li><a href="/spravka">Справочные данные</a> </li>
  <li><a href="/time" >Часовые пояса</a> </li>
  <li><a href="/interactive">Интерактивные задания</a></li>
  <li><a href="/travelling" >Путешествия</a></li>
  <li><a href="/science" class="brd">Наука</a></li>

</ul>

<form class="login" name="log">

  <div class="control-group">
    <label class="control-label" for="inputEmail">Email</label>
    <div class="controls">
      <input type="text" id="inputEmail" placeholder="Email">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="inputPassword">Пароль</label>
    <div class="controls">
      <input type="password" id="inputPassword" placeholder="Пароль">
    </div>
  </div>
  <div class="control-group">
    <div class="controls">
      <label class="checkbox">
        <input type="checkbox"> Запомнить меня
      </label>
      <button type="submit" class="btn">Войти</button>
    </div>
  </div>
</form>



<div class="row">
  <div class="col-xs-6 col-md-3">
    <a href="/fotoalbom" class="thumbnail">
      <img src="image/Nature.jpg" alt="Природа">
    </a>
  </div>

  <div class="col-xs-6 col-md-3">
    <a href="/nature" class="thumbnail">
      <img src="image/Nature.jpg" alt="Природа">
    </a>
  </div>


  <div class="col-xs-6 col-md-3">
    <a href="/adventure" class="thumbnail">
      <img src="image/adventure.jpg"  alt="Приключения">
    </a>
  </div>

</div>
<div class="row">
  <div class="col-xs-6 col-md-3">
    <a href="/spravka" class="thumbnail">
      <img src="image/spravoc.jpg" alt="Справчоные данные">
    </a>
  </div>


  <div class="col-xs-6 col-md-3">
    <a href="/time" class="thumbnail">
      <img src="image/time.jpg"  alt="Часовые пояса">
    </a>
  </div>


  <div class="col-xs-6 col-md-3">
    <a href="/interactive" class="thumbnail">
      <img src="image/game.jpg"  alt="Интерактивные задания">
    </a>
  </div>

</div>
<div class="row">
  <div class="col-xs-6 col-md-3">
    <a href="/travelling" class="thumbnail">
      <img src="image/travelling.jpg"  alt="Путшествия">
    </a>
  </div>


  <div class="col-xs-6 col-md-3">
    <a href="/science" class="thumbnail">
      <img src="image/science.jpg"  alt="Наука">
    </a>
  </div>



</div>



<div class="blok1">
  <h1 class="fraza">Lorem ipsum dolor sit amet, consectetur adipiscing elit.<p>
    Lorem ipsum </h1>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus pulvinar odio vulputate purus fringilla, ullamcorper aliquam ligula rutrum. Aliquam erat volutpat. Sed sem enim, egestas a nisl eget, dignissim pharetra purus. Sed mattis tempus diam vel mattis. In commodo tincidunt nisl, sed viverra tellus tincidunt sit amet. Maecenas gravida massa ligula, eget eleifend risus vestibulum id. In vel semper purus. Duis ac elementum metus, sit amet luctus justo. Sed non blandit leo. Fusce malesuada auctor erat non interdum.

  Duis mattis varius nisl eget tincidunt. Fusce sed interdum libero. Nam hendrerit sem vel tincidunt accumsan. Cras viverra orci sit amet enim venenatis eleifend. Sed diam nunc, maximus nec lorem vitae, egestas semper elit. Donec sagittis, lacus ut semper volutpat, lectus mi viverra erat, eu porta leo nisi non ligula. Aliquam aliquam dui odio, vitae hendrerit neque ultrices a.

  Nunc at urna id sem dapibus tempus nec nec neque. Proin scelerisque enim vel posuere lobortis. Nam vitae magna turpis. Nulla a lacinia ex, imperdiet gravida lorem. Nulla sit amet tristique ex, non pellentesque nisl. Aliquam erat volutpat. Aenean dignissim augue nunc. Phasellus lacinia ipsum vel leo imperdiet laoreet.

  Donec est est, ultricies a dapibus at, luctus id justo. Nam dapibus vitae elit nec maximus. Fusce lacus sapien, luctus vel fermentum non, accumsan ut magna. Aliquam eget elementum velit. Vivamus fermentum gravida quam ac tincidunt. Integer ultrices molestie massa, quis euismod augue consectetur sed. Fusce interdum orci leo, varius fermentum metus vulputate ac. Aliquam ultrices, quam non tempor vehicula, ante lectus aliquam eros, id lobortis ante leo ut dui. Suspendisse rhoncus orci ut ipsum tempor auctor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque consequat accumsan arcu at scelerisque. Quisque faucibus quis erat vel pellentesque. Nunc fermentum ipsum ex, vel tincidunt nulla posuere sit amet. Aenean sagittis lacus eu malesuada venenatis.

  Pellentesque nulla felis, accumsan porta ante at, porttitor efficitur purus. Phasellus a luctus nisl, et egestas magna. Maecenas eget facilisis mauris. Vestibulum imperdiet ornare maximus. Suspendisse maximus eget nisl at viverra. Curabitur venenatis vehicula vulputate. Fusce finibus mi ut dignissim pharetra. Praesent fringilla suscipit ipsum, vitae rhoncus lorem varius sit amet. Suspendisse a tincidunt orci. Phasellus dui turpis, pulvinar scelerisque massa et, fermentum accumsan felis. Donec cursus, turpis ac iaculis facilisis, tortor ipsum tempor lacus, eu mollis nisl diam non dolor. Sed et tellus sit amet dolor tempor blandit. Integer blandit at neque sed consequat. Nam dui eros, vehicula a ultrices sit amet, scelerisque vel tortor. Integer vel gravida lacus. Aliquam nec metus cursus, mollis leo efficitur, consequat orci.</div>


<div class="blok2"><p class="resume">Salavat Shirgaleev 11-405 e-mail: sshirgaleev@gmail.com +7(999)1576037</p> <p class="city">2015<br>Kazan</p></div>






</body>
</html>
