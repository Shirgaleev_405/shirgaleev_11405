<%--
  Created by IntelliJ IDEA.
  User: salavatshirgaleev
  Date: 12.11.15
  Time: 17:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Интерактивные задания</title>
  <link href="/CSS/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<ul id="navbar">
  <li><a href="/index">Главная</a></li>
  <li><a href="/fotoalbom" class="brd">Фотоальбом</a></li>
  <li><a href="/nature">Природа</a></li>
  <li><a href="/adventure">Приключения</a></li>
  <li><a href="/spravka">Справочные данные</a> </li>
  <li><a href="/time" >Часовые пояса</a> </li>
  <li><a href="/interactive">Интерактивные задания</a></li>
  <li><a href="/travelling" >Путешествия</a></li>
  <li><a href="/science" class="brd">Наука</a></li>

</ul>
<div class="opros">
  <form name="test" method="post" action="input">
    <p><b>Ваше имя:</b><br>
      <input type="text" size="40">
    </p>
    <p><b>Самый красивый континет</b><Br>
      <input type="radio" name="continent" value="Europa"> Европа<Br>
      <input type="radio" name="continent" value="Asie">Азия<Br>
      <input type="radio" name="continent" value="South America">Южная Америка<Br>
      <input type="radio" name="continent" value="North America">Северная Америка<Br>
      <input type="radio" name="continent" value="South America">Африка<Br>
      <input type="radio" name="continent" value="Australia">Австралия и Океания<Br>

    </p>
    <p>Что конкретно привлекает<Br>
      <textarea name="comment" cols="40" rows="3"></textarea></p>
    <p><input type="submit" value="Отправить">
      <input type="reset" value="Очистить"></p>
  </form>

</div>

</body>
</html>
