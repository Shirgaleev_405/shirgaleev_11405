<%--
  Created by IntelliJ IDEA.
  User: salavatshirgaleev
  Date: 12.11.15
  Time: 17:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Приключения</title>
    <link href="/CSS/style.css" rel="stylesheet" type="text/css"/>
</head>

<body>
<ul id="navbar">
    <li><a href="/index">Главная</a></li>
    <li><a href="/fotoalbom" class="brd">Фотоальбом</a></li>
    <li><a href="/nature">Природа</a></li>
    <li><a href="/adventure">Приключения</a></li>
    <li><a href="/spravka">Справочные данные</a></li>
    <li><a href="/time">Часовые пояса</a></li>
    <li><a href="/interactive">Интерактивные задания</a></li>
    <li><a href="/travelling">Путешествия</a></li>
    <li><a href="/science" class="brd">Наука</a></li>

</ul>
<div class="priroda">
    <div style="text-align: center;">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/lYjeHvXFhVw" frameborder="0"
                allowfullscreen></iframe>
    </div>
    <h1>Австралиец прошел по канату на 300-метровой высоте
    </h1>

    <div>Экстремал Кейн Питерсен прошел по канату, натянутому на высоте 300 метров над землей. Это рекорд для
        Южного полушария; он был посвящен легендарному канатоходцу из прошлого века.
    </div>
</div>


<div class="priroda">
    <div style="text-align: center;">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/_VPvKl6ezyc" frameborder="0"
                allowfullscreen></iframe>
    </div>
    <h1>Видео: полет вокруг самолета на реактивном ранце</h1>

    <div>Экстремалы полетали вокруг самого крупного пассажирского самолета над Дубаем.</div>
</div>

</body>
</html>