<%--
  Created by IntelliJ IDEA.
  User: salavatshirgaleev
  Date: 12.11.15
  Time: 17:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Справочные данные</title>
  <link href="/CSS/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<ul id="navbar">
  <li><a href="/index">Главная</a></li>
  <li><a href="/fotoalbom" class="brd">Фотоальбом</a></li>
  <li><a href="/nature">Природа</a></li>
  <li><a href="/adventure">Приключения</a></li>
  <li><a href="/spravka">Справочные данные</a> </li>
  <li><a href="/time" >Часовые пояса</a> </li>
  <li><a href="/interactive">Интерактивные задания</a></li>
  <li><a href="/travelling" >Путешествия</a></li>
  <li><a href="/science" class="brd">Наука</a></li>

</ul>

</body>
</html>
