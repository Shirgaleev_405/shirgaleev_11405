package ru.kpfu.itis.SALAVAT_SHIRGALEEV.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by salavatshirgaleev on 16.11.15.
 */
public class AjaxTimeServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/xml");
        response.getWriter().println("<date>" + 42 + "</date>");
    }
}
