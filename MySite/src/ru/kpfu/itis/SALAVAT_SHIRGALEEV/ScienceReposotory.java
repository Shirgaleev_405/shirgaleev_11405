package ru.kpfu.itis.SALAVAT_SHIRGALEEV;

import org.json.JSONArray;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;

/**
 * Created by salavatshirgaleev on 15.11.15.
 */
public class ScienceReposotory {


    public static JSONArray getPoster() {
        HashSet<Science> science = new HashSet<>();
        Connection conn;
        PreparedStatement stmt;
        ResultSet rs;
        JSONArray ja = null;
        try {
            conn = ConnectionClass.returnConnectionClass();
            stmt = conn.prepareStatement("SELECT * FROM POSTER;");
            rs = stmt.executeQuery();
            ja = new JSONArray();
            while (rs.next()) {
                science.add(new Science((new Integer(rs.getString("id"))),
                        (rs.getString("subjects")),
                        (rs.getString("header")),
                        (rs.getString("text"))));
                ja.put(rs.getString("subjects") + "<br></br> " + rs.getString("header") + " " +
                        " " + rs.getString("text") + ";<br/><br/>");
            }
            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return ja;
    }

}
