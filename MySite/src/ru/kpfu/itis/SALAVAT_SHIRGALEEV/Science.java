package ru.kpfu.itis.SALAVAT_SHIRGALEEV;

/**
 * Created by salavatshirgaleev on 15.11.15.
 */
public class Science {
    int id;
    String subjects;
    String header;
    String text;

    public Science(int id, String subjects,String header, String text) {
        this.id = id;
        this.subjects=subjects;
        this.header=header;
        this.text=text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subject) {
        this.subjects = subject;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
