import javax.swing.*;

/**
 * Created by salavatshirgaleev on 11.11.15.
 */
public class FormWithConstructor {
    private JPanel Hello;
    private JButton button1;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FormWithConstructor");
        frame.setContentPane(new FormWithConstructor().Hello);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
