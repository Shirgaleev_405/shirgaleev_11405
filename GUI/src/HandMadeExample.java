import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by salavatshirgaleev on 11.11.15.
 */
public class HandMadeExample extends JFrame {
    public static void main(String[] args) {

        HandMadeExample hm = new HandMadeExample();
        hm.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        hm.setBounds(100, 100, 300, 300);
        hm.setLayout(new GridLayout(2, 1));
        JButton jb = new JButton("PRESS ME");
        hm.add(jb);
        final JTextField tf = new JTextField();
        hm.add(tf);
        tf.setText("Hi, 11-405!");
        jb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((JButton) e.getSource()).setText("PRESSED");
                tf.setText("OOOOOPS!");
            }
        });
        hm.setVisible(true);

    }
}
