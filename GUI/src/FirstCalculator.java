import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by salavatshirgaleev on 11.11.15.
 */
public class FirstCalculator extends JFrame {
    public static void main(String[] args) {


        HandMadeExample hm = new HandMadeExample();
        hm.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        hm.setBounds(100, 100, 300, 300);
        hm.setLayout(new GridLayout(6, 1));
        final JTextField firstfield = new JTextField();
        final JTextField secindfield = new JTextField();
        final JTextField i = new JTextField();
        final JButton add = new JButton("+");
        JButton minus = new JButton("-");
        JButton mult = new JButton("*");

        hm.add(firstfield);
        hm.add(secindfield);
        hm.add(add);
        hm.add(minus);
        hm.add(mult);
        hm.add(i);


        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                i.setText(String.valueOf(Integer.parseInt(firstfield.getText()) + Integer.parseInt(secindfield.getText())));
            }
        });

        minus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                i.setText(String.valueOf(Integer.parseInt(firstfield.getText()) - Integer.parseInt(secindfield.getText())));
            }
        });

        mult.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                i.setText(String.valueOf(Integer.parseInt(firstfield.getText()) * Integer.parseInt(secindfield.getText())));
            }
        });
        hm.setVisible(true);

    }
}
