import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by salavatshirgaleev on 19.11.15.
 */
public class Calculator extends JFrame {

    JTextArea display = new JTextArea();
    JPanel hm = new JPanel(new GridLayout(5, 3));

    final JButton add = new JButton("+");
    final JButton delete = new JButton("C");
    final JButton minus = new JButton("-");
    final JButton mult = new JButton("*");
    final JButton div = new JButton("/");
    final JButton process = new JButton("=");
    final JButton zero = new JButton("0");
    final JButton one = new JButton("1");
    final JButton two = new JButton("2");
    final JButton three = new JButton("3");
    final JButton four = new JButton("4");
    final JButton five = new JButton("5");
    final JButton six = new JButton("6");
    final JButton seven = new JButton("7");
    final JButton eight = new JButton("8");
    final JButton nine = new JButton("9");

    int firstValue = 0;
    int secondValue=0;
    String action;

    Calculator() {

        setBounds(300, 300, 300, 300);

        zero.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (display.getText() != "0") {
                    display.setText(display.getText() + "0");
                }
            }
        });
        one.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText(display.getText() + "1");
            }
        });
        two.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText(display.getText() + "2");
            }
        });
        three.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText(display.getText() + "3");
            }
        });
        four.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText(display.getText() + "4");
            }
        });
        five.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText(display.getText() + "5");
            }
        });
        six.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText(display.getText() + "6");
            }
        });
        seven.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText(display.getText() + "7");
            }
        });
        eight.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText(display.getText() + "8");
            }
        });
        nine.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.setText(display.getText() + "");
            }
        });
        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                firstValue = Integer.parseInt(display.getText());
                action="+";
                display.setText("");
            }
        });
        minus.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                firstValue=Integer.parseInt(display.getText());
                action="-";
                display.setText("");
            }
        });
        mult.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                firstValue= Integer.parseInt(display.getText());
                action="*";
                display.setText("");
            }
        });
        div.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                firstValue=Integer.parseInt(display.getText());
                action="/";
                display.setText("");
            }
        });
        process.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                secondValue = Integer.parseInt(display.getText());
                if (action.equals("+")) {
                    display.setText(String.valueOf(firstValue)+action+String.valueOf(secondValue)+"="+String.valueOf(firstValue+secondValue));
                }
                if (action.equals("-")) {
                    display.setText(String.valueOf(firstValue)+action+String.valueOf(secondValue)+"="+String.valueOf(firstValue-secondValue));
                }
                if (action.equals("*")) {
                    display.setText(String.valueOf(firstValue)+action+String.valueOf(secondValue)+"="+String.valueOf(firstValue*secondValue));
                }
                if (action.equals("/")) {
                    display.setText(String.valueOf(firstValue)+action+String.valueOf(secondValue)+"="+String.valueOf(firstValue/secondValue));
                }
                firstValue=0;
                secondValue=0;
            }
        });


        setLayout(new BorderLayout());
        hm.add(display);
        add(hm);

        hm.add(zero);
        hm.add(one);
        hm.add(two);
        hm.add(three);
        hm.add(four);
        hm.add(five);
        hm.add(six);
        hm.add(seven);
        hm.add(eight);
        hm.add(nine);
        hm.add(delete);

        hm.add(add);
        hm.add(minus);
        hm.add(mult);
        hm.add(div);
        hm.add(process);

        setVisible(true);
    }


    public static void main(String[] args) {

        new Calculator();
    }
}
