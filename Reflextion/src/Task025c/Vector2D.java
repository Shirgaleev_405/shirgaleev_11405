package Task025c;

/**
 * Created by salavatshirgaleev on 30.11.15.
 */
class Vector2D {

    private double x, y;
    @Author(name = "Damir")
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }
    @Author(name = "Damir")
    public double getY() {
        return y;
    }
    @Author(name = "Petr")
    public void setY(double y) {
        this.y = y;
    }
    @Author(name = "Igor'")
    public Vector2D() {
        x = 0;
        y = 0;
    }
    @Author(name = "Maria")
    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }
    @Author(name = "Salavat")
    public Vector2D add(Vector2D v) {
        return new Vector2D(x + v.getX(), y + v.getY());
    }
}
