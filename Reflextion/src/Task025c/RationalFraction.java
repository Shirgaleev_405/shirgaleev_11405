package Task025c;

/**
 * Created by Salavat on 19.11.2014.
 */

public class RationalFraction {
    private int n;
    private int d;

    public RationalFraction() {
        this(0, 0);
    }

    public RationalFraction(int n, int d) {
        this.n = n;
        this.d = d;
    }
    @Author(name = "Igor'")
    public void reduce() {
        int a = Math.abs(this.n);
        int b = Math.abs(this.d);
        int k = 1;

        while (a != 0 && b != 0) {
            if (a > b) {
                a %= b;
            } else {
                b %= a;
            }
            k = a + b;
        }
        this.n = this.n / k;
        this.d = this.d / k;

        if (d < 0) {
            this.d = -this.d;
            this.n = -this.n;
        }

    }
    @Author(name = "Petr")
    public RationalFraction add(RationalFraction k) {
        RationalFraction t = new RationalFraction();
        t.n = this.n * k.d + k.n * this.d;
        t.d = this.d * k.d;
        t.reduce();
        return t;
    }
    @Author(name = "Maria")
    public String toString() {
        return n + "/" + d;
    }
    @Author(name ="Igor'")
    public void add2(RationalFraction k) {
        this.n = this.n * k.d + k.n * this.d;
        this.d = this.d * k.d;
        this.reduce();
    }
    @Author(name ="Maria")
    public RationalFraction sub(RationalFraction k) {
        RationalFraction t = new RationalFraction();
        t.n = this.n * k.d - k.n * this.d;
        t.d = this.d * k.d;
        t.reduce();
        return t;
    }
    @Author(name = "Salavat")
    public void sub2(RationalFraction k) {
        this.n = this.n * k.d - k.n * this.d;
        this.d = this.d * k.d;
        this.reduce();
    }
    @Author(name = "Damir")
    public RationalFraction mult(RationalFraction k) {
        RationalFraction t = new RationalFraction();
        t.n = this.n * k.n;
        t.d = this.d * k.d;
        t.reduce();
        return t;
    }
    @Author(name = "Salavat")
    public void mult2(RationalFraction k) {
        this.n = this.n * k.n;
        this.d = this.d * k.d;
        this.reduce();
    }
    @Author(name = "Petr")
    public RationalFraction div(RationalFraction k) {
        RationalFraction t = new RationalFraction();
        t.n = this.n * k.d;
        t.d = this.d * k.n;
        t.reduce();
        return t;
    }
    @Author(name = "Maria")
    public void div2(RationalFraction k) {
        this.n = this.n * k.d;
        this.d = this.d * k.n;
        this.reduce();
    }
    @Author(name = "Damir")
    public boolean equals(RationalFraction k) {
        this.reduce();
        k.reduce();
        return ((this.n == k.n) && (this.d == k.d));
    }
    @Author(name = "Petr")
    public int numberPart() {
        return (this.n / this.n);
    }
    @Author(name = "Salavat")
    public double value() {
        return (this.n / this.d);
    }
}
