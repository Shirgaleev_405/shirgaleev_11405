package Task025c;

/**
 * Created by salavatshirgaleev on 12.12.15.
 */
public @interface Author {
    String name();
}
