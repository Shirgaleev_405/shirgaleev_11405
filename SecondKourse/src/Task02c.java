import java.util.Random;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.*;

/**
 * Created by salavatshirgaleev on 20.09.15.
 */
public class Task02c {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива");
        int n = scanner.nextInt();
        String[] s = new String[n];
        System.out.println("Введите двоичный код");
        for (int i = 0; i < n; i++) {
            s[i]= String.valueOf(scanner.nextInt());
        }
        int e = 10101;
        Pattern p = Pattern.compile("[0+]|[1+]|[10]+[1]?|[01]+[0]?");

        for (int i=0;i<n;i++) {
            Matcher m = p.matcher(s[i]);
            boolean b = m.matches();
            if (b==true){
                System.out.println(i);
            }
        }

    }
}
