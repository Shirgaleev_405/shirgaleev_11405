import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by salavatshirgaleev on 20.09.15.
 */
public class Task03b {
    public static void main(String[] args) {
        int i = 0, s = 0;
        Pattern p = Pattern.compile("/d[13579]{0}[13579]{0}[13579]{0}");
        while (i < 10) {
            Random random = new Random();
            int n = random.nextInt();

            Matcher m = p.matcher(String.valueOf(n));
            boolean b = m.matches();
            if (b == true) {
                i = i + 1;
                s = s + 1;
            }
        }
        System.out.println(i + " " + s);

        Random random = new Random();

        i = 0;
        s = 0;

        while (i < 10) {
            int n = random.nextInt();
            Matcher m = p.matcher(String.valueOf(n));
            while (m.find()) {

                i = i + 1;

                s = s + 1;
            }
        }
        System.out.println(i + " " + s);
    }
}
