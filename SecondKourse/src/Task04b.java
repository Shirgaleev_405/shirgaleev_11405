import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by salavatshirgaleev on 20.09.15.
 */
public class Task04b {
    public static void main(String[] args) {
        Random random = new Random();
        int i=0;
        int s=0;
        while (i<10){
            int n=random.nextInt();
            Pattern p= Pattern.compile("[2468]{1}+[2468]{1}+[2468]{1}+[2468]{0,1}+[2468]{0,1}+[2468]{0,1}");
            Matcher m = p.matcher(String.valueOf(n));
            boolean b = m.matches();
            if (b==true){
                i=i+1;
                s=s+1;
            }
        }

        System.out.println(i+" "+s);


    }
}
