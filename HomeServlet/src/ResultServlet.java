import com.sun.tools.doclets.formats.html.markup.DocType;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by salavatshirgaleev on 03.10.15.
 */
@WebServlet(name = "ResultServlet")
public class ResultServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProcessServlet.class.getTypeParameters();
        PrintWriter pw = response.getWriter();
        response.setContentType("text/html");
        pw.println("<!DOCTYPE html>");
        pw.println("<html>\n" +
                "<head>\n" +
                "<title>Result</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "select" +
                "<br>\n" +
                "Меня зовут (здесь впишите Ваше имя), это моя первая страничка!\n" +
                "</body>\n" +
                "</html>");
        pw.close();
    }
}
