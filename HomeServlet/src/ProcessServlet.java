import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.stream.events.Characters;
import java.io.*;
import java.util.StringTokenizer;

/**
 * Created by salavatshirgaleev on 02.10.15.
 */
@WebServlet(name = "ProcessServlet")
public class ProcessServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
        request.getParameter("select");
        request.getParameter("myText");
        int r=0;

        if (request.getParameter("select") == "Characters") {
            r = request.getParameter("myText").length();
            response.sendRedirect("/result");
        }
        if (request.getParameter("select") == "Number of words") {
            StringTokenizer ins = new StringTokenizer(request.getParameter("myText"));

            while (ins.hasMoreTokens()) {
                r++;
            }
            response.sendRedirect("/result");
        }

        if (request.getParameter("myText") == "Number of proposals") {
            String line = null;
            BufferedReader bufferedReader = null;
            BufferedReader buf = new BufferedReader(new FileReader(request.getParameter("myText")));
            try {
                while (buf.readLine() != null) {
                    r++;
                    bufferedReader.close();
                }
            } catch (Exception e) {
            }
            response.sendRedirect("/result");
        } else {
            r = 89;
            response.sendRedirect("/result");
        }
        doGet(request,response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String select = request.getParameter("select");
        String myText = request.getParameter("myText");
        HttpSession session =request.getSession();
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        response.setContentType("text/html");
        pw.print("<!DOCTYPE HTML>\n" +
                "<html>\n" +
                " <head>\n" +
                "  <meta charset=\"utf-8\">\n" +
                "  <title>Analyse</title>\n" +
                " </head>\n" +
                " <body>\n" +
                "  <form action=\"doPost\" method=\"get\">\n" +
                "    <p><textarea rows=\"10\" cols=\"45\" name=\"myText\"></textarea></p>\n" +
                "<select>\n" +
                "<option value='Characters'>Characters "+getInitParameter("select")+"</option> \n" +
                "<option value='words'>Number of words </option> \n" +
                "<option value='proposals'>Number of proposals </option> \n" +
                "<option value='paragraphs'>Number of paragraphs </option> \n" +
                "</select>\n" +
                "    <p><input type=\"submit\" value=\"Process\"></p>\n" +
                "  </form>\n" +
                "\n" +
                " </body>\n" +
                "</html>");
        pw.close();


        int r=0;

        if (request.getParameter("select") == "Characters") {
            r = request.getParameter("myText").length();
            String characters=toString();
            session.setAttribute("select",r);
            response.sendRedirect("/result");
        }
        if (request.getParameter("select") == "Number of words") {
            StringTokenizer ins = new StringTokenizer(request.getParameter("myText"));

            while (ins.hasMoreTokens()) {
                r++;
            }
            session.setAttribute("words",r);
            response.sendRedirect("/result");
        }

        if (request.getParameter("myText") == "Number of proposals") {
            String line = null;
            BufferedReader bufferedReader = null;
            BufferedReader buf = new BufferedReader(new FileReader(request.getParameter("myText")));
            try {
                while (buf.readLine() != null) {
                    r++;
                    bufferedReader.close();
                }
            } catch (Exception e) {
            }
            session.setAttribute("proposals",r);
            response.sendRedirect("/result");
        } else {
            r = 89;
            session.setAttribute("paragraphs",r);
            response.sendRedirect("/result");
        }


    }
}
