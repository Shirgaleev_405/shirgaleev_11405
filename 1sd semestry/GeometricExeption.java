/**
 * Created by Salavat on 05.12.2014.
 */
class GeometricException extends Exception{
    public GeometricException(String message) {
        super(message);
    }
}
