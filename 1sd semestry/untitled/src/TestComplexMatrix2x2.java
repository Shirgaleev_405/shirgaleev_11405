/**
 * Created by Salavat on 07.12.2014.
 */
public class TestComplexMatrix2x2 {
    ComplexNumber a = new ComplexNumber(1, 6);
    ComplexNumber b = new ComplexNumber(8, 5);
    ComplexNumber c = new ComplexNumber(5, 9);
    ComplexNumber d = new ComplexNumber(4, 6);
    ComplexNumber e = new ComplexNumber(6, 8);
    ComplexNumber f = new ComplexNumber(3, 1);
    ComplexNumber g = new ComplexNumber(3, 7);
    ComplexNumber h = new ComplexNumber(7, 2);
    ComplexVector2D n = new ComplexVector2D();
    ComplexMatrix2x2 p = new ComplexMatrix2x2(a, b, c, d);
    ComplexMatrix2x2 y = new ComplexMatrix2x2(e, f, g, h);

}
