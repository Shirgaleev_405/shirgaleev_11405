/**
 * Created by Salavat on 07.12.2014.
 */
public class RationalComplexNumber {
    private RationalFraction a;
    private RationalFraction bi;
    public RationalFraction getA(){
        return this.a;
    }
    public RationalFraction getBi(){return this.bi;}
    public void setA(RationalFraction a){
        this.a = a;
    }
    public void setBi(RationalFraction bi){
        this.bi = bi;
    }
    public RationalComplexNumber(){
        this(new RationalFraction(),new RationalFraction());
    }
    public RationalComplexNumber(RationalFraction t1, RationalFraction t2){
        this.a = t1;
        this.bi = t2;
    }
    public RationalComplexNumber add(RationalComplexNumber s1){
        RationalComplexNumber c = new RationalComplexNumber();
        c.a = this.a.add(s1.a);
        c.bi = this.bi.add(s1.bi);
        return c;
    }
    public RationalComplexNumber sub(RationalComplexNumber s1){
        RationalComplexNumber c = new RationalComplexNumber();
        c.a = this.a.sub(s1.a);
        c.bi = this.bi.sub(s1.bi);
        return c;
    }
    public RationalComplexNumber mult(RationalComplexNumber s1){
        RationalComplexNumber c = new RationalComplexNumber();
        c.a = this.a.mult(s1.a).sub(this.bi.mult(s1.bi));
        c.bi = this.a.mult(s1.bi).add(this.bi.mult(s1.a));
        return c;
    }
    public String toString() {
        this.a.reduce();
        this.bi.reduce();
        if (this.bi.getN() > 0)
            return a + "+" + bi + "i";
        else
            return a + "" + bi + "i";
    }
}
