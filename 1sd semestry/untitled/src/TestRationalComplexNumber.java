/**
 * Created by Salavat on 07.12.2014.
 */
public class TestRationalComplexNumber {
    public static void main(String[] args) {
        RationalFraction n1 = new RationalFraction(3,4);
        RationalFraction n2 = new RationalFraction(6,7);
        RationalFraction n3 = new RationalFraction(5,4);
        RationalFraction n4 = new RationalFraction(4,3);
        RationalComplexNumber s1 = new RationalComplexNumber(n1,n2);
        RationalComplexNumber s2 = new RationalComplexNumber(n3,n4);

        System.out.println(s1.add(s2));
        System.out.println(s1.sub(s2));
        System.out.println(s1.mult(s2));
    }
}