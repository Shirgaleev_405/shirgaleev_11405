/**
 * Created by Salavat on 07.12.2014.
 */
public class RationalComplexMatrix2x2 {
    RationalComplexNumber a[][] = new RationalComplexNumber[2][2];
    public RationalComplexMatrix2x2(){
        this(new RationalComplexNumber());
    }
    public RationalComplexMatrix2x2(RationalComplexNumber b){
        this(b,b,b,b);
    }
    public RationalComplexMatrix2x2(RationalComplexNumber x1,RationalComplexNumber x2,RationalComplexNumber x3,RationalComplexNumber x4){
        a[0][0] = x1;
        a[0][1] = x2;
        a[1][0] = x3;
        a[1][1] = x4;
    }
    public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 matrix){
        RationalComplexMatrix2x2 c = new RationalComplexMatrix2x2();
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                c.a[i][j] = this.a[i][j].add(matrix.a[i][j]);
        return c;
    }
    public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 matrix){
        RationalComplexMatrix2x2 c = new RationalComplexMatrix2x2();
        for (int t = 0; t < 2; t++)
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                    c.a[t][i] = c.a[t][i].add(this.a[t][j].mult(matrix.a[j][i]));
        return c;
    }
    public RationalComplexNumber det(){
        RationalComplexNumber c1 = this.a[0][0].mult(this.a[1][1]);
        RationalComplexNumber c2 = this.a[1][0].mult(this.a[0][1]);
        return c1.sub(c2);
    }
    public RationalComplexVector2D multVector(RationalComplexVector2D vector){
        RationalComplexVector2D c = new RationalComplexVector2D();
        c.setS1(vector.getS1().mult(this.a[0][0]).add(vector.getS2().mult(this.a[0][1])));
        c.setS2(vector.getS1().mult(this.a[1][0]).add(vector.getS2().mult(this.a[1][1])));
        return c;
    }
    public String toString() {
        return a[0][0] + " " + a[0][1] + "\n" + a[1][0] + " " + a[1][1];
    }
}

