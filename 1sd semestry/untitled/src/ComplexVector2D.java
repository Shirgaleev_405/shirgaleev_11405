/**
 * Created by Salavat on 21.11.2014.
 */
public class ComplexVector2D {
    private ComplexNumber s1;
    private ComplexNumber s2;
    public ComplexNumber getS1(){
        return this.s1;
    }
    public ComplexNumber getS2(){
        return this.s2;
    }
    public void setS1(ComplexNumber s1){
        this.s1 = s1;
    }
    public void setS2(ComplexNumber s2){
        this.s2 = s2;
    }
    public ComplexVector2D(){
        this(new ComplexNumber(),new ComplexNumber());
    }
    public ComplexVector2D(ComplexNumber s1, ComplexNumber s2){
        this.s1 = s1;
        this.s2 = s2;
    }
    public ComplexVector2D add(ComplexVector2D a1){
        ComplexVector2D c = new ComplexVector2D();
        c.s1 = this.s1.add(a1.s1);
        c.s2 = this.s2.add(a1.s2);
        return c;
    }
    public String toString(){
        return "{ " + this.s1 + " , " + this.s2 + " }";
    }
    public ComplexNumber ScalarProduct(ComplexVector2D a1){
        return this.s1.multi(a1.s1).add(this.s2.multi(a1.s2));
    }
    public boolean equals(ComplexVector2D a1){
        return this.s1.equals(a1.s1) && this.s2.equals(a1.s1);
    }
}
