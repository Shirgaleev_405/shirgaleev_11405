/**
 * Created by Salavat on 06.12.2014.
 */
public class RationalMatrix2x2 {
    private RationalFraction a[][] = new RationalFraction[2][2];

    public RationalMatrix2x2() {
        this(new RationalFraction());
    }

    public RationalMatrix2x2(RationalFraction b) {
        this(b,b,b,b);
    }

    public RationalMatrix2x2(RationalFraction x1, RationalFraction x2, RationalFraction x3, RationalFraction x4) {
        a[0][0] = x1;
        a[0][1] = x2;
        a[1][0] = x3;
        a[1][1] = x4;
    }

    public RationalMatrix2x2 add(RationalMatrix2x2 b) {
        RationalMatrix2x2 c = new RationalMatrix2x2();
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                c.a[i][j] = this.a[i][j].add(b.a[i][j]);
        return c;
    }

    public RationalMatrix2x2 mult(RationalMatrix2x2 b) {
        RationalMatrix2x2 c = new RationalMatrix2x2();
        for (int t = 0; t < 2; t++)
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)
                    c.a[t][i] = c.a[t][i].add(this.a[t][j].mult(b.a[j][i]));
        return c;
    }

    public RationalFraction det() {
        RationalFraction c1 = this.a[0][0].mult(this.a[1][1]);
        RationalFraction c2 = this.a[0][1].mult(this.a[1][0]);
        return c1.sub(c2);

    }

    public RationalVector2D multVector(RationalVector2D n) {
        RationalVector2D c = new RationalVector2D();
        c.setX(n.getX().mult(this.a[0][0]).add(n.getY().mult(this.a[0][1])));
        c.setX(n.getX().mult(this.a[1][0]).add(n.getY().mult(this.a[1][1])));
        return c;
    }

    public String toString() {
        return a[0][0] + " " + a[0][1] + "\n" + a[1][0] + " " + a[1][1];
    }
}
