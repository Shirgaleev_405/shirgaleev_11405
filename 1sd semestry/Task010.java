import java.util.Scanner;

/**
 * Created by Салават on 20.09.2014.
 */
public class Task010 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = 0;
        y = x > 2 ? (x * x - 1) / (x + 2) : x <= 0 ? (x * x) * (2 * x + 1) : (x * x - 1) * (x + 2);
        System.out.println(y);
    }
}
