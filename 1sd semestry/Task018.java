import java.util.Scanner;

/**
 * Created by Салават on 24.09.2014.
 */
import java.util.Scanner;

public class Task018 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        double x = s.nextDouble();
        int n = s.nextInt();
        double c = 0;
        double a = s.nextDouble();
        c += a;
        for (int i = 1; i <= n; i++) {
            c *= x;
            a = s.nextDouble();
            c += a;
        }
        System.out.println(c);
    }
}
