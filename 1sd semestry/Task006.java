/**
 * @author Salavat Shirgaleev
 *         405
 *         006
 */
public class Task006 {
    public static void main(String[] args) {
        int p;
        int x = 3;
        p = x + 6;
        p = p * x + 10;
        p = p * x + 25;
        p = p * x + 30;
        p = p * x + 101;
        System.out.println(p);
    }
}
