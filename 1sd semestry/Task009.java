import java.util.Scanner;

/**
 * Created by Салават on 20.09.2014.
 */
public class Task009 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = 0;
        if (x > 2) {
            y = (x * x) / (x + 2);
        } else if (x > 0) {
            y = (x * x - 1) * (x + 2);
        } else y = x * x * (1 + 2 * x);
        System.out.println(y);

    }
}
