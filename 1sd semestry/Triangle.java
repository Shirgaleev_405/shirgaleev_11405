/**
 * Created by Salavat on 04.12.2014.
 */
public class Triangle implements Perimetr, Measurable {
    private int a, b, c;

    public Triangle(int a, int b, int c) throws GeometricException {
        if ((a < 0) || (b < 0) || (c < 0)) {
            throw new GeometricException("Недопустимое(-ые) значениие(-ия)");
        }
        if (((a + b) < c) || ((a + c) < c) || ((b + c) < a)) {
            throw new GeometricException("Недопустимое(-ые) значениие(-ия)");
        }
        this.a = a;
        this.b = b;
        this.c = c;
    }


    @Override
    public double measurable() {
        return Math.sqrt((a + b + c) * (a + b) * (a + c) * (b + c));
    }

    @Override
    public double perimetr() {
        return a + b + c;
    }
}
