import java.util.Scanner;

/**
 * Created by Салават on 24.09.2014.
 */
public class Task022 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 1; i <= 2 * n + 1; i++) {
            for (int i2 = 1; i2 <= 2 * n + 1; i2++) {
                if ((i2 - (n + 1)) * (i2 - (n + 1)) + (i - n - 1) * (i - n - 1) <= n * n) {
                    System.out.print(0);
                } else {
                    System.out.print("*");
                }

            }
            System.out.println();
        }
    }
}
