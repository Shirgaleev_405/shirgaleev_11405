import java.util.Scanner;

/**
 * Created by Салават on 11.10.2014.
 */
public class Task031 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        boolean s = true;
        int r = 0;
        int p=1;
        int[] a = new int[n];
        for (int i = 0; i < a.length; i++) {
            a[i] = scanner.nextInt();
        }


        for (int i = 0; i < a.length && s; i += 3) {
            if (a[i] % 3 == 0) {
                s = true;
            } else {
                s = false;
            }
        }

        for (int i = 0; i < a.length; i++) {

            if (a[i] > 0) {
                if (s) {
                    r = r+a[i];
                } else {
                    p = p*a[i];
                }
            }
        }
        if(r==0){
            System.out.println(p);
        }
        else {
            System.out.println(r);
        }

    }
}
