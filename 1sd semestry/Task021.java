import java.util.Scanner;

/**
 * Created by Салават on 24.09.2014.
 */
public class Task021 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int i, i2, i3;
        for (i = 1; i <= n; i++) {
            for (i2 = 1; i2 <= 2 * n - i; i2++) {
                System.out.print(" ");
            }
            for (i3 = 1; i3 <= 2 * i - 1; i3++) {
                System.out.print("*");
            }
            System.out.println();
        }
        for (i = 1; i <= n; i++) {
            for (i2 = 1; i2 <= n - i; i2++) {
                System.out.print(" ");
            }
            for (i3 = 1; i3 <= 2 * i - 1; i3++) {
                System.out.print("*");
            }
            for (i2 = 1; i2 <= 2 * n - 2 * i + 1; i2++) {
                System.out.print(" ");
            }
            for (i3 = 1; i3 <= 2 * i - 1; i3++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
