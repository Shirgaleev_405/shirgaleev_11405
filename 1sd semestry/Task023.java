/**
 * @author Shirgaleev Salavat
 *         405
 *         023
 */
public class Task023 {
    public static void main(String[] args) {
        final double EPS = 1e-9;
        double sum = 0;
        double dev = 1;
        double num = 0;
        double den = 0;
        for (int n = 1; dev >= EPS; n++) {
            num = 2 * n + 3;
            den = 5 * n * n * n * n + 1;
            dev = num / den;
            sum = sum + dev;
        }

        System.out.println(sum);
    }
}
