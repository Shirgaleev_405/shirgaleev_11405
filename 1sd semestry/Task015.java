import java.util.Scanner;

/**
 * Created by Салават on 22.09.2014.
 */
public class Task015 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int n = scanner.nextInt();
        int i;
        double p = n + x;
        for (i = n; i >= 1; i--) {
            p = i + n / p;
        }
        System.out.println(p);
    }
}