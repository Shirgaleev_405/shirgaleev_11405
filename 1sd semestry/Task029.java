import java.util.Scanner;

/**
 * Created by Салават on 11.10.2014.
 */
public class Task029 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int n = scanner.nextInt();
        int i = 1;
        int q = 1;
        int s = 1;
        while (n > 0) {
            q = q + s * n % 10;
            n = n / 10;
            s = s * k;
        }
        System.out.println(q);
    }
}
