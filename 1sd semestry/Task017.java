import java.util.Scanner;

/**
 * Created by Salavat Shirgaleev on 23.09.2014.
 */
public class Task017 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double f = 1;
        double f1 = 2;
        double p = f * f / f1;
        for (int i = 2; i <= n; i++) {
            f = f * (i - 1);
            f1 = f1 * 2 * i * (2 * i - 1);
            p = p + f * f / f1;
        }
        System.out.println(p);
        scanner.close();
    }


}