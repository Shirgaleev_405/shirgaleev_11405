import java.util.Scanner;

/**
 * Created by Салават on 11.10.2014.
 */
public class Task027 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        boolean pr = false;
        int i = 1;
        while ((i <= n) && !pr) {
            int q = scanner.nextInt();
            pr =  (q % 6== 0) || (q % 5 == 0);
            i++;
        }
        System.out.println(pr);
    }
}
