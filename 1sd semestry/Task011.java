import java.util.Scanner;

/**
 * Created by Салават on 20.09.2014.
 */
public class Task011 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = 1;
        for (int i = n % 2 + 2; i <= n; i += 2) {
            p *= 1;
        }
        System.out.println(p);
    }
}
