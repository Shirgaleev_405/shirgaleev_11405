import java.util.Scanner;

/**
 * Created by Salavat on 06.11.2014.
 */
public class Task047 {
    public static int add(int k, int i) {
        int p;
        p = i * k;
        return p;
    }

    public static void f(double x) {
        final double EPS = 1e-9;
        double sum = 0;
        double dev = 1;
        int i = 1;
        double f = x - 1;
        for (int n = 1; dev >= EPS; n++, i++) {
            i = 9 * i;
            f = f * f * n;
            dev = n * i * f;
            sum = sum + (1 / dev);
        }
        System.out.println(sum);
    }

    public static double s(int n, int a[], int b[]) {
        double s = 0;
        for (int i = 0; i < n; i++) {
            s += a[i] * b[i];
        }
        return s;
    }

    public static double cos(int n, int a[], int b[]) {
        double s = 0;
        double s1 = 0;
        double s2 = 0;

        for (int i = 0; i < n; i++) {
            s += a[i] * b[i];
            s1 += a[i] * a[i];
            s2 += b[i] * b[i];
        }

        double cos;
        cos = s / (Math.sqrt(s1) * Math.sqrt(s2));
        return cos;
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int k = scanner.nextInt();
        int x = scanner.nextInt();
        for (int i = 1; i <= 9; i++) {
            System.out.println(i + " * " + k + " = " + add(k, i));
        }
        f(x);
        int n = scanner.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        int[] b = new int[n];
        for (int i = 0; i < n; i++) {
            b[i] = scanner.nextInt();
        }
        System.out.println(s(n, a, b));
        System.out.println(cos(n,a,b));

    }


}
