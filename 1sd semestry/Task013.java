import java.util.Scanner;

/**
 * Created by Салават on 21.09.2014.
 */
public class Task013 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double r = 1;
        int i = 1;
        while (i <= n) {
            r = r * (4 * i * i) / (2 * i - 1) / (2 * i + 1);
            i = i + 1;
        }
        System.out.println(r);
    }
}
