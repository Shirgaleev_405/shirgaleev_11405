/**
 * @author Salavat Shirgaleev
 *         405
 *         007
 */
public class Task007 {
    public static void main(String[] args) {
        int a = 5;
        int b = 3;
        System.out.println("a+b=" + (a + b));
        System.out.println("a-b=" + (a - b));
        System.out.println("b-a=" + (b - a));
        System.out.println("a*b=" + (a * b));
        System.out.println("a/b=" + (a / b));
        System.out.println("a%b=" + (a % b));
        System.out.println("b/a=" + (b / a));
        System.out.println("b%a=" + (b % a));
    }
}
