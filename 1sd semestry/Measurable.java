/**
 * Created by Salavat on 05.12.2014.
 */
public interface Measurable {
    double measurable();
}
