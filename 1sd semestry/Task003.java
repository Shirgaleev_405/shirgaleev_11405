/**
 * @author Salavat Shirgaleev
 *         405
 *         003
 */
public class Task003 {
    public static void main(String[] args) {
        int R = 3;
        System.out.println(Math.PI * 4 / 3 * R * R * R);
    }
}
