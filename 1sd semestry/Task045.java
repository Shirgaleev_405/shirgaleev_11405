import java.util.Scanner;

/**
 * @author Salavat Shirgaleev
 *
 */
public class Task045 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        n = n + 1;
        String[] a = new String[n];
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextLine();
        }
        System.out.println();
        int k = scanner.nextInt();
        k = k + 1;
        String[] b = new String[k];
        for (int j = 0; j < k; j++) {
            b[j] = scanner.nextLine();
        }
        System.out.println();
    }
}