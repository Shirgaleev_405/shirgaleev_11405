import java.util.Scanner;

/**
 * Created by Салават on 23.09.2014.
 */
public class Task016 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int n = scanner.nextInt();
        int s = 0;
        int i = 1;
        int a = 1;
        while (i <= n) {
            a = a * (x + i);
            s = s + a;
            i = i + 1;
        }
        System.out.println(s);
    }
}
