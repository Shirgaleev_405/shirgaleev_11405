import java.util.Scanner;

/**
 * Created by Салават on 24.09.2014.
 */
public class Task020 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int i = 1; i <= 2 * n + 1; i++) {
            if (i < n + 1) {
                for (int i2 = 1; i2 <= n - i + 1; i2++) {
                    System.out.print("*");
                }
                for (int i2 = 1; i2 <= 2 * i - 1; i2++) {
                    System.out.print("0");
                }
                for (int i2 = 1; i2 <= n - i + 1; i2++) {
                    System.out.print("*");
                }
                System.out.println();
            } else {
                if (i == n + 1) {
                    for (int i2 = 1; i2 <= 2 * n + 1; i2++) {
                        System.out.print("0");
                    }
                    System.out.println();
                } else {
                    for (int i2 = 1; i2 <= i - n - 1; i2++) {
                        System.out.print("*");
                    }
                    for (int i2 = 1; i2 <= (2 * n + 1 - i) * 2 + 1; i2++) {
                        System.out.print("0");
                    }
                    for (int i2 = 1; i2 <= i - n - 1; i2++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }
        }
    }
}


