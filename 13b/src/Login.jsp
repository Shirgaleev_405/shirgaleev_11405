<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>

<%--
  Created by IntelliJ IDEA.
  User: Shirgaleev Salavat
  Date: 12.10.2015
  Time: 15:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%Map<String, String> logpas = new HashMap<String, String>();%>
<%logpas.put("Student", "123456");%>
<%logpas.put("Teacher", "654321");%>
<%logpas.put("OutUser", "qwerty");%>

<html>
<head>
    <title>LoginPage</title>
</head>
<body>

<%if (request.getMethod().equals("GET")) {%>
<%String user = (String) session.getAttribute("username");%>
<%if (user != null) {%>
<%response.sendRedirect("/profile");%>
<%} else {%>
<form method='POST' action='/login'>
    <input type='text' name='username'/>
    <input type='text' name='password'/>
    <input type='submit' value='LogIn'/>
    <label for='rem'>Запомнить</label>
    <input type='checkbox' name='remember' id='rem' value='remembered'/>
</form>
<%}%>

<%if (request.getParameter("remember").equals("checked")) {%>
<%Cookie cookie = new Cookie(request.getParameter("username"), request.getParameter("password"));%>
<%cookie.setMaxAge(24 * 60 * 60 * 30);%>
<%response.addCookie(cookie);%>
<%}%>
<%}%>

<%if (request.getMethod().equals("POST")) {%>
  <%session.setAttribute("username", request.getParameter("username"));%>
  <%String username = request.getParameter("username");%>
  <%String password = request.getParameter("password");%>
  <%if (username != null) { %>
    <%response.sendRedirect("/profile");%>
  <%} else {%>
    <%if (logpas.containsKey(request.getParameter("username")) && logpas.get(username).equals(password)) {%>
      <%session.setAttribute("username", username);%>
      <%response.sendRedirect("/profile");%>
    <%} else {%>
      <%if (!logpas.containsKey(request.getParameter("username"))) {%>
        Error! Not correct username..
        <%request.setAttribute("username", username);%>
        <%response.sendRedirect("/login");%>
      <%}%>
      <%if (logpas.containsKey(request.getParameter("username")) && !logpas.get(username).equals(password)) {%>
        Error! Not correct password...
        <%request.setAttribute("username", username);%>
        <%response.sendRedirect("/login");%>
      <%}%>
    <%}%>
  <%}%>
<%}%>

</body>
</html>
