/**
 * Created by salavatshirgaleev on 07.12.15.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Dots {
    public static void main(String[] args) {
        new DotsWin();
    }
}

class DotsWin extends JFrame {
    private int counter = 0;
    private Rectangle[][] dots = new Rectangle[7][7];
    private int[][] status = new int[7][7];

    DotsWin() {
        setTitle("Точки!");
        setLocation(new Point(500, 200));
        setSize(500, 400);
        setResizable(false);
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                dots[i][j] = new Rectangle(40 + 70 * i, 70 + 40 * j, 11, 11);
                status[i][j] = 0;
            }
        }
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {

            }
        }
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    Point pointMouse = e.getPoint();
                    for (int i = 0; i < 7; i++) {
                        for (int j = 0; j < 7; j++) {
                            if (dots[i][j].contains(pointMouse) && (status[i][j] == 0)) {
                                if (counter % 2 == 0) {
                                    status[i][j] = 1;
                                } else {
                                    status[i][j] = 2;
                                }
                                counter++;
                                repaint();
                            }
                        }
                    }
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        addWindowListener(new WindowCatcher());
        setVisible(true);
    }

    public void paint(Graphics g) {
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (status[i][j] == 0) {
                    g.setColor(Color.black);

                } else if (status[i][j] == 1) {
                    g.setColor(Color.red);
                } else {
                    g.setColor(Color.blue);
                }
                g.fillRect(dots[i][j].x, dots[i][j].y, dots[i][j].width, dots[i][j].height);

            }
        }
        paintVertically(g);
        paintHorizontal(g);

    }

    public void paintLines(Graphics g) {
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if ((j + 1 < 7) && status[i][j] == status[i][j + 1] && status[i][j] != 0) {
                    if (status[i][j] == 1) {
                        g.setColor(Color.red);
                    } else {
                        g.setColor(Color.blue);
                    }
                    g.drawLine(dots[i][j + 1].x, dots[i][j + 1].y, dots[i][j].x, dots[i][j].y);
                }
                if ((i - 1 <= 0) && status[i][j] == status[i - 1][j]) {
                    if (status[i][j] == 1) {
                        g.setColor(Color.red);
                    } else {
                        g.setColor(Color.blue);
                    }
                    g.drawLine(dots[i - 1][j].x, dots[i - 1][j].y, dots[i][j].x, dots[i][j].y);
                }
                if ((j - 1 <= 0) && status[i][j] == status[i][j - 1]) {
                    if (status[i][j] == 1) {
                        g.setColor(Color.red);
                    } else {
                        g.setColor(Color.blue);
                    }
                    g.drawLine(dots[i][j - 1].x, dots[i][j - 1].y, dots[i][j].x, dots[i][j].y);
                }
                if ((i + 1 < 7) && status[i][j] == status[i + 1][j]) {
                    if (status[i][j] == 1) {
                        g.setColor(Color.red);
                    } else {
                        g.setColor(Color.blue);
                    }
                    g.drawLine(dots[i + 1][j].x, dots[i + 1][j].y, dots[i][j].x, dots[i][j].y);
                }
            }
        }
    }

    public void paintVertically(Graphics g) {
        int i0 = 0;
        int j0 = 0;
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                if (status[i][j] == status[i0][j0]) {
                    if (status[i][j] == 1) {
                        g.setColor(Color.red);
                    } else {
                        g.setColor(Color.blue);
                    }
                    if (status[i][j] != 0) {
                        g.drawLine(dots[i0][j0].x, dots[i0][j0].y, dots[i][j].x, dots[i][j].y);
                    }
                }
                j0 = j;
            }
            i0 = i;
        }
    }

    public void paintHorizontal(Graphics g) {
        int i0 = 0;
        int j0 = 0;
        for (int j = 0; j < 7; j++) {
            for (int i = 0; i < 7; i++) {
                if (status[i][j] == status[i0][j0]) {
                    if (status[i][j] == 1) {
                        g.setColor(Color.red);
                    } else {
                        g.setColor(Color.blue);
                    }
                    if (status[i][j] != 0) {
                        g.drawLine(dots[i0][j0].x, dots[i0][j0].y, dots[i][j].x, dots[i][j].y);
                    }
                }
                i0 = i;
            }
            j0 = j;
        }
    }


    class WindowCatcher extends WindowAdapter {
        public void windowClosing(WindowEvent event) {
            event.getWindow().dispose();
            System.exit(0);
        }
    }


}

