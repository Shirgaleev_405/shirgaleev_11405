import javax.swing.*;
import javax.swing.plaf.metal.MetalBorders;
import javax.swing.text.html.Option;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by salavatshirgaleev on 16.11.15.
 */
public class Saper extends JFrame {
    private int size;
    private int minesNumber;
    private int buttonSize = 50;
    private int[][] field;
    int winner = 0;
    int reset = JOptionPane.YES_OPTION;

    public Saper(int s, int i1) {


        size = s;
        minesNumber = i1;

        setLayout(new GroupLayout(getContentPane()));
        generateField();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                final JButton jb = new JButton();
                jb.setBounds(j * buttonSize, i * buttonSize, buttonSize, buttonSize);
                jb.addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (e.getButton() == MouseEvent.BUTTON3) {
                            JButton jb1 = (JButton) e.getSource();

                            if (jb1.getText().equals("P")) {
                                jb1.setText("");
                            } else {
                                if (!jb1.isEnabled()) {
                                    jb1.setText(jb1.getText());
                                } else {
                                    jb1.setText("P");
                                }
                            }


                        }
                        if (e.getButton() == MouseEvent.BUTTON1) {

                            JButton jb1 = (JButton) e.getSource();
                            int x = jb1.getX() / buttonSize;
                            int y = jb1.getY() / buttonSize;
                            if (field[y][x] == 1) {
                                jb1.setText("X");
                            } else {
                                if (countMinesAround(y, x) == 0) {
                                    jb1.setText("");
                                } else {
                                    int count = countMinesAround(y, x);
                                    jb1.setText(count + "");
                                }
                            }
                            jb1.setEnabled(false);
                            if (jb.getText().equals("X")) {
                                JOptionPane.showOptionDialog(null, "You are lose! Do you want to start new game?", "WARNING", JOptionPane.YES_NO_OPTION,
                                        JOptionPane.QUESTION_MESSAGE,
                                        null, null, null);
                                setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                            }
                            if (!jb.getText().equals("X")) {

                                if (win()) {
                                    JOptionPane.showOptionDialog(null, "You are winner! Do you want to start new game?",
                                            "Grats!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

                                }
                            }


                        }
                    }

                    @Override
                    public void mousePressed(MouseEvent e) {

                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {

                    }

                    @Override
                    public void mouseExited(MouseEvent e) {

                    }
                });
                add(jb);
            }
        }

        setBounds(0, 0, buttonSize * size + 50, buttonSize * size + 50);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setVisible(true);

    }

    public boolean win() {
        winner++;
        if (winner == size * size - minesNumber) {
            return true;
        } else {
            return false;
        }
    }


    public void open() {
        for (int i = -1; i < 2; i++) {
            for (int j = -1; j < 2; j++) {
                if ((field[getX() + i][getY() + j] != 1) && (getX() != 0 && getX() != size && getY() != 0 && getY() != size)) {
                    field[getY() + i][getX() + j] = Character.forDigit(field[getY() + i][getX() + j], size);
                }
            }
        }

    }

    public int getPosition(int x, int y) {
        return field[getX()][getY()];
    }


    public void openMine() {
        for (int i = 1; i < size; i++) {
            for (int j = 1; j < size; j++) {
                if (field[i][j] == 1) {
                    field[i][j] = '*';
                }
            }
        }

        openMine();
    }


    private int countMinesAround(int x, int y) {
        int s = 0;
        for (int i = -1; i <= 1; i++) {
            if (x + i >= 0 && x + i < size) {
                for (int j = -1; j <= 1; j++) {
                    if (y + j >= 0 && y + j < size) {
                        s += field[x + i][y + j];
                    }
                }
            }
        }
        return s;
    }

    private void generateField() {
        field = new int[size][size];
        Random r = new Random();
        int k = 0;
        while (k < minesNumber) {
            int i = r.nextInt(size);
            int j = r.nextInt(size);
            if (field[i][j] == 0) {
                field[i][j] = 1;
                k++;
            }
        }
    }


    public static void main(String[] args) {

        new Saper(3, 1);
    }
}