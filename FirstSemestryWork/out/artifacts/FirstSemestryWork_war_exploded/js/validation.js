$(document).ready(function () {


    $('#firstname').blur(function () {
        if ($(this).val() != '') {
            var p = /[a-zA-Z]$/i;
            if (p.test($(this).val())) {
                $(this).css({'border': '1px solid #444444'});
                $('#validfirstname').text('True');
            } else {
                $(this).css({'border': '1px solid #ff0000'});
                $('#validfirstname').text('Error');
            }
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#validfirstname').text('Obligatory filling');
        }
    });

    $('#lastname').blur(function () {
        if ($(this).val() != '') {
            var p = /[a-zA-Z]$/i;
            if (p.test($(this).val())) {
                $(this).css({'border': '1px solid #444444'});
                $('#validlastname').text('True');
            } else {
                $(this).css({'border': '1px solid #440000'});
                $('#validlastname').text('Error');
            }
        } else {
            $(this).css({'border': '1px solid #440000'});
            $('#validlastname').text('Obligatory filling');
        }
    });
    $('#login').blur(function () {
        if ($(this).val() != '') {
            var p = /[a-zA-Z0-9-_.]$/i;
            if (p.test($(this).val())) {
                $(this).css({'border': '1px solid #444444'});
                $('#validlogin').text('True');
            } else {
                $(this).css({'border': '1px solid #440000'});
                $('#validlogin').text('Error');
            }
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#validlogin').text('Obligatory filling');
        }
    });
    $('#password').blur(function () {
        if ($(this).val() != '') {
            var p = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/i;
            if (p.test($(this).val())) {
                $(this).css({'border': '1px solid #444444'});
                $('#validpassword').text('True');
            }
        } else {
            $(this).css({'border': '1px solid #40000'});
            $('#validpassword').text('obligatory filling');
        }
    });
    $('#city').blur(function () {
        if ($(this).val() != '') {
            var p = /[a-zA-Z]$/i;
            if (p.test($(this).val())) {
                $(this).css({'border': '1px solid #444444'});
                $('#validcity').text('True');
            } else {
                $(this).css({'border': '1px solid #440000'});
                $('#validcity').text('Error');
            }
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#validcity').text('Obligatory filling');
        }
    });
    $('#country').blur(function () {
        if ($(this).val() != '') {
            var p = /[a-zA-Z]$/i;
            if (p.test($(this).val())) {
                $(this).css({'border': '1px solid #444444'});
                $('#validcity').text('True');
            } else {
                $(this).css({'border': '1px solid #440000'});
                $('#validcity').text('Error!');
            }
        } else {
            $(this).css({'border': '1px solid #ff0000'});
            $('#validcity').text('obligatory filling');
        }
    });


    $('#age').blur(function () {
        if ($(this).val() != '') {
            var p = /^(?:1(?:00?|\d)|[2-5]\d|[6-9]\d?)$/i;
            if (p.test($(this).val())) {
                $(this).css({'border': '1px solid #444444'});
                $('#validage').text('True');
            } else {
                $(this).css({'border': '1px solid #440000'});
                $('#validage').text('Try again');
            }
        } else {
            $(this).css({'border': '1px solid #440000'});
            $('#valiage').text('Obligatory filling');
        }
    });

})
;