<!doctype html>
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" src="/Jquery/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="/js/country.js"></script>
    <link rel="stylesheet" href="/css/home.css" type="text/css"/>
    <title>Country</title>
</head>
<body>
<div id="page">

    <div id="logo">
        <h1><a href="/home" id="logoLink">First Geographic site</a></h1>
    </div>
    <div id="nav">
        <ul>
            <li><a href="/home">Home</a></li>
        </ul>
    </div>

    <input type="text" id="s" oninput="f()"/>

    <div id="res"></div>

    <div id="footer">
        <p>
            Created by Shirgaleev Salavat
        </p>
    </div>
</div>
</body>
</html>