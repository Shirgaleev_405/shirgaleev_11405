<!doctype html>
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" src="/Jquery/jquery-2.1.4.min.js"></script>
    <link rel="stylesheet" href="/css/home.css" type="text/css"/>
    <title>Log in</title>

</head>
<body>

<div id="page">
    <div id="logo">
        <h1><a href="/home" id="logoLink">First geographic site</a></h1>
    </div>
    <div id="nav">
        <ul>
            <li><a href="/logout">Logout</a></li>
            <li><a href="/home">Home</a></li>
        </ul>
    </div>
    <div id="content">
        <form  role="form" action='/login' method='post'>
            <input type="text"  name="login" placeholder="Login" value='${login}' >
            <input type="password" name="password"  placeholder="Password" >
            <label>
                <input id="remember me" type="checkbox" checked name="check"> Remember me
            </label>
            <button  type="submit">Log in</button>
        </form>
    </div>
    <div id="footer">
        <p>
            Created by Shirgaleev Salavat
        </p>
    </div>
</div>
</body>
</html>
