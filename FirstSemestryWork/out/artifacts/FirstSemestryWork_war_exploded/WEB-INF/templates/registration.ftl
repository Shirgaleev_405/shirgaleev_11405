<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" src="/Jquery/jquery-2.1.4.min.js"></script>
    <link rel="stylesheet" href="/css/home.css" type="text/css"/>
    <script type="text/javascript" src="/js/validation.js"></script>
    <title>Registration</title>

</head>
<body>
<div id="page">

    <div id="logo">
        <h1><a href="/" id="logoLink">First geographic site</a></h1>
    </div>

    <div id="nav">
        <ul>
            <li><a href="/login">Log in</a></li>
            <li><a href ="/home">Home</a></li>

        </ul>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

<#if error?has_content >
    <div class="alert alert-danger alert-dismissable" align="center">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error!</strong> ${error}
    </div>
</#if>

    <form action="/registration" method="post">


        <div align="center">
            <b> Имя:</b>

            <input type='text' id="firstname" name="firstname" required="required"/>
            <span id="validfirstname"></span>
        </div>

        <div align="center">
            <b> Фамилия:</b>

            <input type='text' id="lastname " name="lastname" required="required"/>
            <span id="validlastname"></span>
        </div>

        <div align="center">
            <b>Логин:</b>

            <input type='text' id="login" name="login" required="required"/>
            <span id="validlogin"></span>
        </div>

        <div align="center">
            <b> Пароль:</b>

            <input type='text' id="password" name="password" required="required"/>
            <span id="validpassword"></span>
        </div>



        <div align="center">
            <b> Город:</b>

            <input type='text' id="city" name="city" required="required"/>
            <span id="city"></span>
        </div>

        <div align="center">
            <b> Страна:</b>

            <input type='text' id="country" name="country" required="required"/>
            <span id="country"></span>
        </div>

        <div align="center">
            <b> Сколько Вам лет?</b>

            <input type='text' id="age" name="age" required="required"/>
            <span id="validage"></span>
        </div>
        <center><input  type="submit" value="Registration"/></center>
    </form>


    <div id="footer">
        <p>
            Created by Shirgaleev Salavat
        </p>
    </div>
</div>
</body>
</html>