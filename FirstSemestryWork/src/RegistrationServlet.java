import Repository.Config;
import Repository.UserRepository;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by salavatshirgaleev on 16.12.15.
 */
@WebServlet(name = "RegistrationServlet")
public class RegistrationServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        Configuration cfg = Config.getConfig(getServletContext());
        Template tmpl = cfg.getTemplate("registration.ftl");
        HashMap<String, Object> root = new HashMap<>();
        if (request.getSession().getAttribute("error_msg") != null) {
            root.put("error", request.getSession().getAttribute("error_msg").toString());
            request.getSession().setAttribute("error_msg", null);
        }
        if (request.getSession().getAttribute("login") != null) {
            root.put("login", request.getSession().getAttribute("login"));
        } else {
            root.put("login", "");
        }
        if (request.getSession().getAttribute("password") != null) {
            root.put("password", request.getSession().getAttribute("name"));
        } else {
            root.put("password", "");
        }
        if (request.getSession().getAttribute("firstname") != null) {
            root.put("firstname", request.getSession().getAttribute("firstname"));
        } else {
            root.put("firstname", "");
        }
        if (request.getSession().getAttribute("lastname") != null) {
            root.put("lastname", request.getSession().getAttribute("lastname"));
        } else {
            root.put("lastname", "");
        }
        if (request.getSession().getAttribute("city") != null) {
            root.put("city", request.getSession().getAttribute("city"));
        } else {
            root.put("city", "");
        }

        if (request.getSession().getAttribute("country") != null) {
            root.put("country", request.getSession().getAttribute("country"));
        } else {
            root.put("country", "");
        }
        if (request.getSession().getAttribute("age") != null) {
            root.put("age", request.getSession().getAttribute("age"));
        } else {
            root.put("age", "");
        }
        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");

        if (!UserRepository.isUserWithThisLogin(request.getParameter("login"))) {

            try {
                UserRepository.addUser(request.getParameter("login"), request.getParameter("password"), request.getParameter("firstname"),
                        request.getParameter("lastname"), request.getParameter("country"), request.getParameter("city"),
                        request.getParameter("age"));

            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            Cookie myCookie = new Cookie("login", request.getParameter("login"));
            myCookie.setMaxAge(100 * 60);
            response.addCookie(myCookie);
            request.getSession().setAttribute("login", request.getParameter("login"));
            request.getSession().setAttribute("firstname", request.getParameter("firstname"));
            response.sendRedirect("/home");
        } else {
            request.getSession().setAttribute("error_msg", "The user with such login already exists");
            if (!request.getParameter("password").isEmpty()) {
                request.getSession().setAttribute("password", request.getParameter("password"));
            }
            if (!request.getParameter("firstname").isEmpty()) {
                request.getSession().setAttribute("name", request.getParameter("firstname"));
            }
            if (!request.getParameter("lastname").isEmpty()) {
                request.getSession().setAttribute("lastname", request.getParameter("lastname"));
            }
            if (!request.getParameter("city").isEmpty()) {
                request.getSession().setAttribute("city", request.getParameter("city"));
            }


            if (!request.getParameter("age").isEmpty()) {
                request.getSession().setAttribute("age", request.getParameter("age"));
            }
            if (!request.getParameter("country").isEmpty()) {
                request.getSession().setAttribute("country", request.getParameter("country"));
            }
            response.sendRedirect("/registration");
        }
    }
}
