import Objects.User;
import Repository.Config;
import Repository.UserRepository;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by salavatshirgaleev on 15.12.15.
 */
@WebServlet(name = "ProfileServlet")
public class ProfileServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        Cookie[] cookies = request.getCookies();
        String user = (String) request.getSession().getAttribute("current_user");
        Cookie name = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("login")) {
                    name = cookie;
                    break;
                }
            }
        }
        if ((request.getSession().getAttribute("login") != null) || (name != null)) {
            try {
                Configuration cfg = Config.getConfig(getServletContext());
                Template tmpl = cfg.getTemplate("profile.ftl");
                HashMap<String, Object> root = new HashMap<>();
                if (user != null) {
                    User currentUser = (UserRepository.getUsers(request.getSession().getAttribute("login").toString()));
                    root.put("lastname", currentUser.getLastName());
                    root.put("login", currentUser.getLogin());
                    root.put("firstname", currentUser.getFirstName());
                    root.put("country", currentUser.getCountry());
                    root.put("city", currentUser.getCity());
                    root.put("age", currentUser.getAge());


                } else  {
                    User currentUser = (UserRepository.getUsers(name.getValue().toString()));
                    root.put("lastname", currentUser.getLastName());
                    root.put("login", currentUser.getLogin());
                    root.put("firstname", currentUser.getFirstName());
                    root.put("country", currentUser.getCountry());
                    root.put("city", currentUser.getCity());
                    root.put("age",currentUser.getAge());
                }
                tmpl.process(root, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        } else {
            response.sendRedirect("/login");
        }
    }
}
