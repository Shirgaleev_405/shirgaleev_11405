package Repository;

import Objects.User;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by salavatshirgaleev on 15.12.15.
 */
public class UserRepository {
    public static Connection conn;
    public static PreparedStatement stmt;

    public static User getUsers(String login) {

        try {
            conn = Connect.returnConnection();
            ResultSet rs;
            User user = new User();
            PreparedStatement stmt = conn.prepareStatement("SELECT login, pass, firstname , lastname , " +
                    "city , country, age   " +
                    "FROM users WHERE  login = ? ");
            stmt.setString(1, login);
            rs = stmt.executeQuery();
            if (rs.next()) {
                user.setLogin(rs.getString(1));
                user.setPassword(rs.getString(2));
                user.setFirstName(rs.getString(3));
                user.setLastName(rs.getString(4));
                user.setCity(rs.getString(5));
                user.setCountry(rs.getString(6));
                user.setAge(Integer.parseInt(rs.getString(7)));
            }
            stmt.close();
            return user;
        } catch (SQLException e1) {
            e1.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static String checkPassword(String password) {
        MessageDigest messageDigest;
        byte[] checkYou = new byte[0];
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(password.getBytes());
            checkYou = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return new String(checkYou);
    }

    public static void addUser(String login, String password, String fistName, String lastName, String country,
                               String city, String age) throws SQLException, ClassNotFoundException {
        Connection conn;
        PreparedStatement stmt;
        try {


            conn = Connect.returnConnection();
            stmt = conn.prepareStatement("INSERT INTO Users(pass,login,  firstname,lastname,country,city," +
                    " age) VALUES(?,?,?,?,?,?,?)");
            stmt.setString(1, checkPassword(password));
            stmt.setString(2, login);
            stmt.setString(3, fistName);
            stmt.setString(4, lastName);
            stmt.setString(5, country);
            stmt.setString(6, city);
            stmt.setString(7, age);
            stmt.executeUpdate();
            stmt.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static String md5Custom(String st) {
        MessageDigest messageDigest;
        byte[] hp = new byte[0];
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(st.getBytes());
            hp = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return new String(hp);

    }

    public static int isUser(String login, String password) {

        try {
            Connection conn = Connect.returnConnection();
            ResultSet rs;
            PreparedStatement stmt = conn.prepareStatement("SELECT pass FROM users WHERE login =?");
            stmt.setString(1, login);
            rs = stmt.executeQuery();
            if (rs.next()) {
                if (rs.getString("pass").equals(md5Custom(password))) {
                    return 2;
                } else {
                    return 1;
                }
            } else {
                return 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static boolean isUserWithThisLogin(String login) {
        try {
            int s = 0;
            Class.forName("org.postgresql.Driver");

            Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/DataBase", "postgres",
                    "salavat96A"
            );
            Statement stmt = conn.createStatement();
            ArrayList<String> logs = new ArrayList<>();
            ResultSet rs = stmt.executeQuery("SELECT login FROM users ");
            //ResultSet rs;
            //PreparedStatement stmt = conn.prepareStatement("SELECT login FROM users ");
            //stmt.setString(1, login);
            //rs = stmt.executeQuery();
            while (rs.next()) {
                logs.add(rs.getString("login"));
            }
            for (int i = 0; i < logs.size(); i++) {
                if (logs.get(i).equals(login)) {
                    s++;
                }
            }
            if (s > 0) {
                return true;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static boolean loginIsUse(String login) {
        Connection conn;
        PreparedStatement stmt;
        ArrayList<String> us = new ArrayList<>();
        ResultSet rs;
        try {
            conn = Connect.returnConnection();
            stmt = conn.prepareStatement("SELECT l] FROM users;");
            rs = stmt.executeQuery();
            while (rs.next()) {
                us.add(rs.getString("login"));
            }
            stmt.close();
            for (int i = 0; i < us.size(); i++) {
                if (us.get(i).equals(login)) {
                    return true;
                }
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
