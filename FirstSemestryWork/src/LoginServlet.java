import Repository.Config;
import Repository.UserRepository;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by salavatshirgaleev on 15.12.15.
 */
@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        Cookie[] cookies = request.getCookies();
        Cookie username = null;
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("login")) {
                    username = cookie;
                    break;
                }
            }
        }

        if ((username != null)) {
            response.sendRedirect("/home");
        } else {
            try {
                Configuration cfg = Config.getConfig(getServletContext());
                Template tmpl = cfg.getTemplate("login.ftl");
                HashMap<String, Object> root = new HashMap<>();
                if (request.getSession().getAttribute("login") != null) {
                    root.put("login", request.getSession().getAttribute("login").toString());
                } else {
                    root.put("login", "");
                }

                if (request.getSession().getAttribute("error_msg") != null) {
                    root.put("error", request.getSession().getAttribute("error_msg").toString());
                    request.getSession().setAttribute("error_msg", null);
                }

                tmpl.process(root, response.getWriter());
            } catch (TemplateException e) {
                e.printStackTrace();
            }

        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        request.setCharacterEncoding("utf-8");
        int k = (UserRepository.isUser(request.getParameter("login"), request.getParameter("password")));
        if (k == 2) {
            if (request.getParameter("check") != null) {
                Cookie myCookie = new Cookie("login", request.getParameter("login"));
                myCookie.setMaxAge(100 * 60);
                response.addCookie(myCookie);
            }
            session.setAttribute("login", request.getParameter("login"));
            session.setAttribute("author","true");
            response.sendRedirect("/home");
        } else {
            if (k == 1) {
                session.setAttribute("error_msg", "Incorrect password");
            } else {
                session.setAttribute("error_msg", "Incorrect login");
            }
            session.setAttribute("login", request.getParameter("login"));
            response.sendRedirect("/login");
        }
    }
}
