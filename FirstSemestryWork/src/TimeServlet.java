import Repository.Config;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by salavatshirgaleev on 17.12.15.
 */
@WebServlet(name = "TimeServlet")
public class TimeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Configuration cfg = Config.getConfig(getServletContext());
        Template tmpl = cfg.getTemplate("time.ftl");
        HashMap<String, Object> root = new HashMap<>();
        root.put("g", "'I wish it need not have happened in my time, said Frodo." +
                "So do I, said Gandalf, and so do all who live to see such times." +
                " But that is not for them to decide. All we have to decide is what to " +
                "do with the time that is given us.' ");

        root.put("author","-J.R.R. Tolkien, The Fellowship of the Ring");
        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }

    }
}
