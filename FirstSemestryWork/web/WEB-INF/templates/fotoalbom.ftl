<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" src="/Jquery/jquery-2.1.4.min.js"></script>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <link rel="stylesheet" href="/css/home.css" type="text/css"/>
    <script type="text/javascript" src="/js/bootstrap-carousel.js"></script>
    <title>Home</title>

</head>
<body>


<div id="page">

    <div id="logo">
        <h1><a href="/home" id="logoLink">First Geographic site</a></h1>
    </div>
    <div id="nav">
        <ul>
            <li><a href="/home">Home</a></li>
        </ul>
    </div>
    <br>
    <br><br>
    <br><br>
    <br><br>
    <br>
    <h1 align="center"> ${info} </h1>


<div class="container">

    <div id="carousel1" class="carousel slide " data-interval="false" data-ride="carousel">
        <!--Slide-->
        <div class="carousel-inner">
            <div class="item active" align="center">
                <img src="/albom/baykal.jpg" width="1280" height="720">
            </div>
            <div class="item" align="center">
                <img src="/albom/colorado.jpg" width="1280" height="720">
            </div>
            <div class="item" align="center">
                <img src="/albom/grandcanyon.jpg" width="1280" height="720">
            </div>

            <div class="item" align="center">
                <img src="/albom/toratay.jpg" width="1280" height="720">
            </div>
            <div class="item" align="center">
                <img src="/albom/yellowstone.jpg" width="1280" height="720">
            </div>
            <div class="item" align="center">
                <img src="/albom/yazyk.jpg" width="1280" height="720">
            </div>
            <div class="item" align="center">
                <img src="/albom/geizer.jpg" width="1280" height="720">
            </div>

        </div>


        <!--Controls-->
        <a href="#carousel1" class="carousel-control left" data-slide="prev">
            &lsaquo;
        </a>
        <a href="#carousel1" class="carousel-control right" data-slide="next">
            &rsaquo;
        </a>
    </div>
</div>
    <br/>

</div>
<div id="footer">
    <p>
        Created by Shirgaleev Salavat
    </p>
</div>
</div>


</body>
</html>
