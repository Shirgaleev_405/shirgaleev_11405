<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <script type="text/javascript" src="/Jquery/jquery-2.1.4.min.js"></script>
    <link rel="stylesheet" href="/css/home.css" type="text/css"/>
    <title>Home</title>

</head>
<body>

<div id="page">
    <div id="logo">
        <h1><a href="/home" id="logoLink">First geographical site</a></h1>
    </div>
    <div id="nav">
        <ul>
            <li><a href="/profile">Profile</a></li>
            <li><a href="/logout">Logout</a></li>
            <li><a href="/fotoalbom">PhotoAlbom</a></li>
            <li><a href="/time"> Time</a> </li>

        </ul>
    </div>
    <div id="content">
        <h2>Home</h2>
        <p>
            Love, having no geography, knows no boundaries.
        </p>

        <p>
        <div align="right"> Truman Capote</div>
        </p>
    </div>
    <div id="footer">
        <p>
            Created by Shirgaleev Salavat
        </p>
    </div>
</div>
</body>
</html>
