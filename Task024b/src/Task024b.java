import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by salavatshirgaleev on 25.11.15.
 */
public class Task024b {
    static String file = "File.html";

    public static void main(String[] args) throws IOException {

        URL url = new URL("https://ru.wikipedia.org/wiki/Ишимбай");
        InputStream s1 = new BufferedInputStream(url.openStream());
        FileOutputStream f1 = new FileOutputStream(new File(file));
        int a = s1.read();
        long time = System.nanoTime();
        while (a != -1) {
            try {
                f1.write(a);
                a = s1.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Scanner page = new Scanner(new File(file));
        Pattern pdf = Pattern.compile("http.*/(.*).pdf");
        Pattern mp3 = Pattern.compile("http.*/(.*).mp3");
        while (page.hasNext()) {
            Matcher txt = pdf.matcher(page.nextLine());
            Matcher mus = mp3.matcher(page.nextLine());
            if (txt.find()) {
                String res = txt.group();
                URL u = new URL(res);
                InputStream on = new BufferedInputStream(u.openStream());
                OutputStream off = new BufferedOutputStream(new FileOutputStream(new File(txt.group(1)+".pdf")));
                int b = on.read();
                while (b != -1) {
                    try {
                        off.write(b);
                        b = on.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                on.close();
                off.close();
            }
            if (mus.find()) {
                String res = mus.group();
                URL u = new URL(res);
                InputStream on = new BufferedInputStream(u.openStream());
                OutputStream off = new BufferedOutputStream(new FileOutputStream(new File(mus.group(1)+".mp3")));
                int b = on.read();
                while (b != -1) {
                    try {
                        off.write(b);
                        b = on.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                on.close();
                off.close();
            }
        }
        page.close();
        s1.close();
        f1.close();
        time = System.nanoTime() - time;
        System.out.println(time);
    }
}
